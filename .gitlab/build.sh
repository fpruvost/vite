#!/usr/bin/env bash
###
#
#  @file build.sh
#  @copyright 2023-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
#                       Univ. Bordeaux. All rights reserved.
#
#  @version 1.2.0
#  @author Mathieu Faverge
#  @author Florent Pruvost
#  @date 2023-12-07
#
###
set -ex

# To be removed when OTF2 will be "correctly" installed in the Docker image
PATH=$PATH:$OTF2_DIR/bin

#
# Build the project
#
if [[ "$SYSTEM" != "windows" ]]; then
    if [[ "$SYSTEM" == "macosx" ]]; then
        echo "macos unsupported"
        return
        # if brew ls --versions scotch > /dev/null; then
        #   echo "Scotch is already installed with brew";
        # else
        #   echo "Start installing Scotch with brew";
        #   brew install scotch;
        # fi
        # if brew ls --versions starpu > /dev/null; then
        #   echo "Starpu is already installed with brew";
        # else
        #   echo "Start installing Starpu with brew";
        #   brew install --build-from-source ~/brew-repo/starpu.rb;
        # fi
        # # clang is used on macosx and it is not compatible with MORSE_ENABLE_COVERAGE=ON
        # # to avoid the Accelerate framework and get Openblas we use BLA_PREFER_PKGCONFIG
        # # we do not have parsec installed on the macosx machine
        # cmake -B build -S . -DVITE_CI_VERSION=${VERSION} -DVITE_CI_BRANCH=${BRANCH} \
            #       -C .gitlab/ci-test-initial-cache.cmake \
            #       -DMORSE_ENABLE_COVERAGE=OFF -DBLA_PREFER_PKGCONFIG=ON
    else
        cmake -B build -S . -DVITE_CI_VERSION=${VERSION} -DVITE_CI_BRANCH=${BRANCH} \
              -C .gitlab/ci-test-initial-cache.cmake
    fi
else
    echo "windows unsupported"
    return
    # on windows the mpi_f08 interface is missing, see https://www.scivision.dev/windows-mpi-msys2/
    # default scotch in windows msys2 is int32
    # do not use static libraries because executables are too large and the build
    # directory can reach more than 10Go
    # cmake -GNinja -B build -S . -DCMAKE_INSTALL_PREFIX=$PWD/install-${VERSION} \
        #       -DBUILD_SHARED_LIBS=ON
fi
cmake --build build -j 4
cmake --install build

