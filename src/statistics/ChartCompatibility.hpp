/**
 *
 * @file src/statistics/ChartCompatibility.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 *
 * @date 2024-07-17
 */
/*!
 *\file ChartCompatibility.hpp
 */
#pragma once

#include <QtGlobal>
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
#include <QtCharts/qchart.h>
using namespace QtCharts;
#endif
