/**
 *
 * @file src/common/Message.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 * @author Augustin Degomme
 * @author Clement Vuchener
 *
 * @date 2024-07-17
 */
#ifndef MESSAGE_HPP
#define MESSAGE_HPP

/*!
 * \class Message
 * \brief Contains routine for easily send messages of information, warnings or errors to the user
 */

class stringstream;
class ostream;
#include <sstream>
#include "Log.hpp"

class Message : public std::stringstream
{
private:
    static Message *_message;
    static Log *_log_support;

    Message();

public:
    static const class end_error_t
    {
    } ende;
    static const class end_warning_t
    {
    } endw;
    static const class end_information_t
    {
    } endi;

    static Message *get_instance();
    static void kill();

    static void set_log_support(Log *);

    /**
     * \brief Only used to define operator overloading on <<
     */
    static Log *get_log_support();
};

std::ostream &operator<<(std::ostream &, Message::end_error_t);
std::ostream &operator<<(std::ostream &, Message::end_warning_t);
std::ostream &operator<<(std::ostream &, Message::end_information_t);

#endif
