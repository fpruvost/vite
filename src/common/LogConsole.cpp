/**
 *
 * @file src/common/LogConsole.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 *
 * @date 2024-07-17
 */
/*!
 *\file LogConsole.cpp
 */

#include <iostream>
#include <string>
/* -- */
#include "LogConsole.hpp"

void LogConsole::error(const std::string &s) const {
    std::cerr << "ERROR: " << s << std::endl;
}

void LogConsole::warning(const std::string &s) const {
    std::cerr << "WARNING: " << s << std::endl;
}

void LogConsole::information(const std::string &s) const {
    std::cerr << "INFO:" << s << std::endl;
}
