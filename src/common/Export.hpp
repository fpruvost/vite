/**
 *
 * @file src/common/Export.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 *
 * @date 2024-08-02
 */

/*!
 *\file Export.hpp
 *\brief This class defines everything used to export a trace.
 */

#include <QObject>
/* -- */
#include "trace/Trace.hpp"
#include "trace/Variable.hpp"

class RenderLayout;

class Export : QObject
{
    Q_OBJECT
private:
    /*!
     * \brief Export a counter
     * \param var The variable to export
     * \param filename The filename in which to export the variable
     */
    void export_variable(Variable *var, const std::string &filename);

public:
    /*!
     * \brief Default constructor
     */
    Export() = default;

    /*!
     * \brief Default constructor
     */
    ~Export() = default;

    /*!
     * \brief Export a trace to a svg file
     * \param trace The trace to export
     * \param path_to_export The path corresponding to the output svg path
     * \param time_start The visible start of the trace
     * \param time_end The visible end of the trace
     */
    void export_trace_to_svg(Trace *trace, const std::string &path_to_export, double time_start, double time_end);

    /*!
     * \brief Export a trace to a svg file
     * \param trace The trace to export
     * \param path_to_export The path corresponding to the output svg path
     * \param interval Interval of the visible trace
     */
    void export_trace_to_svg(Trace *trace, const std::string &path_to_export, Interval interval);

    void open_export_window(QWidget *parent, RenderLayout *render_layout);

    void open_export_variable_window(const std::map<std::string, Variable *> &variable_list, const std::string &export_filename);
};
