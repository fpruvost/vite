/**
 *
 * @file src/common/common.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Arthur Redondy
 * @author Mathieu Faverge
 * @author Thibault Soucarre
 * @author Jule Marcoueille
 * @author Olivier Lagrasse
 * @author Augustin Degomme
 *
 * @date 2024-07-17
 */
/*!
 *\file common.hpp
 *\brief This file gives some global defines and typedef.
 */

#ifndef COMMON_HPP
#define COMMON_HPP

#include "ViteConfig.hpp"

/*!
 * \def VITE_ORGANISATION_NAME
 * \brief the ViTE string
 */
#define VITE_ORGANISATION_NAME "ViTE"

/*!
 * \def VITE_ORGANISATION_DOMAIN
 * \brief the ViTE string
 */
#define VITE_ORGANISATION_DOMAIN "vite.gforge.inria.fr"

/*!
 * \def VITE_APPLICATION_NAME
 * \brief the ViTE string
 */
#define VITE_APPLICATION_NAME "vite"

/*!
 * \def VITE_WEBSITE
 * \brief the ViTE website (string)
 */
#define VITE_WEBSITE "https://solverstack.gitlabpages.inria.fr/vite"

/*!
 * \brief Unity for count elements such as number of states or events... NOT USED YET
 */
typedef long Element_count;

/*!
 * \brief Unity for variables of position of a element in the display such as height, abscissa...
 */
typedef float Element_pos;

/*!
 * \brief Unity for colors
 */
typedef float Element_col;

/*!
 * \brief Define the unity of time.
 */
typedef double Times;

/*!
 * \def M_PI
 * \brief PI if not defined (mscv for example)
 */
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

/* TODO : move this somewhere else when we will have the message handler */
#define vite_warning(str) std::cerr << str << std::endl;
#define vite_error(str) std::cerr << str << std::endl;

#endif /* COMMON_H */
