/**
 *
 * @file src/common/LogConsole.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 *
 * @date 2024-07-17
 */
/*!
 *\file LogConsole.hpp
 */
#ifndef LOGCONSOLE_HPP
#define LOGCONSOLE_HPP

#include "Log.hpp"

class LogConsole : public Log
{

public:
    /*!
     * \brief The function takes a string then displayed it into the terminal, then killed the application.
     * \param string The string to be displayed.
     */
    void error(const std::string &string) const override;

    /*!
     \brief The function takes a string then displayed it into the terminal, then the program go on with an indeterminated behaviour.
     \param string The string to be displayed.
     */
    void warning(const std::string &string) const override;

    /*!
     * \brief The function takes a strings then displayed it into the terminal, then the program go on.
     * \param string The string to be displayed.
     */
    void information(const std::string &string) const override;
};

#endif
