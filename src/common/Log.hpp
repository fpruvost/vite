/**
 *
 * @file src/common/Log.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Olivier Lagrasse
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
/*!
 *\file Log.hpp
 */

#ifndef LOG_HPP
#define LOG_HPP

/*!
 *\brief  This is an interface, used by the terminal and graphical interfaces to display log information for the user.
 *
 *Log defines functions implemented in their inherited classes. It gives functions which can be used by others parts of ViTE (for example the Parser and the Data Structure). Thus, it hides which kind of interface is used: a console interface (where messages are displayed in the terminal) or a graphical interface (where messages are displayed in a dialog box).
 */
class Log
{

public:
    /*!
     * \arg string : the string to be displayed.
     * \brief The function takes a string then displayed it either on the terminal if there is an Interface_console instance, or on a dialog box for the Interface_graphic. Then, it killed the application.
     */
    virtual void error(const std::string &) const = 0;

    /*!
     \arg string : the string to be displayed.
     \brief The function takes a string then displayed it either on the terminal if there is an Interface_console instance, or on a dialog box for the Interface_graphic. Then the program go on, but with an indeterminated behaviour.
     */
    virtual void warning(const std::string &) const = 0;

    /*!
     \arg string : the string to be displayed.
     \brief The function takes a string then displayed it either on the terminal if there is an Interface_console instance, or on a dialog box for the Interface_graphic. Then the program go on normally.
     */
    virtual void information(const std::string &) const = 0;
};

#endif
