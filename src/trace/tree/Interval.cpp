/**
 *
 * @file src/trace/tree/Interval.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Kevin Coulomb
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */

#include <string>
/* -- */
#include "trace/values/Value.hpp"
#include "trace/values/Date.hpp"
/* -- */
#include "trace/tree/Interval.hpp"

Interval::Interval() :
    _left(0.0), _right(0.0) {
}

Interval::Interval(Date left, Date right) :
    _left(std::move(left)), _right(std::move(right)) {
}
