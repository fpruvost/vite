/**
 *
 * @file src/trace/values/Value.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 * @author Thibault Soucarre
 * @author Olivier Lagrasse
 * @author Augustin Degomme
 *
 * @date 2024-07-17
 */
#ifndef VALUE_HPP
#define VALUE_HPP

/*!
 *
 * \file Value.hpp
 *
 */

#include <string>

/*!
 *
 * \class Value
 * \brief Abstract class to store calue used in the trace
 *
 */
class Value
{
protected:
    /*!
     * boolean at true if the value set is correct.
     */
    bool _is_correct;

public:
    virtual ~Value() = default;

    /*!
     *
     * \fn is_correct() const = 0
     * \brief Return if the value set is correct.
     * \return the value is correct.
     *
     */
    bool is_correct() const { return _is_correct; }

    /*!
     *
     * \fn is_correct() const = 0
     * \brief Return if the value set is correct.
     * \return the value is correct.
     *
     */
    void set_correct(bool corr) { _is_correct = corr; }
    /*!
     *
     * \fn to_string() const = 0
     * \brief a to string method.
     * \return the value in a string format.
     *
     */
    virtual std::string to_string() const = 0;

    /*!
     * \brief The precision for printing double with std::cout or printf.
     */
    static const int _PRECISION = 15;
};

#endif // VALUE_HPP
