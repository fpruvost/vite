/**
 *
 * @file src/trace/values/Date.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 * @author Thibault Soucarre
 * @author Clement Vuchener
 *
 * @date 2024-07-17
 */
#ifndef DATE_HPP
#define DATE_HPP

/*!
 *
 * \file Date.hpp
 *
 */

#include "Value.hpp"

/*!
 *
 * \class Date
 * \brief Store a string in the trace
 *
 */
class Date : public Value
{
private:
    double _value { 0.0 };

public:
    /*!
     * \brief Constructor
     */
    Date();

    /*!
     * \brief Constructor
     */
    Date(double);

    /*!
     * \brief Constructor
     */
    Date(const std::string &value);

    /*!
     *
     * \fn to_string() const
     * \return a string of the date
     */
    std::string to_string() const override;

    /*!
     *
     * \fn get_value() const
     * \return the value
     */
    double get_value() const;

    /*!
     *
     * \fn operator< (const Date &) const
     * \brief Compare the date
     * \return true if the Date is lower than this.
     *
     */
    bool operator<(const Date &) const;

    /*!
     *
     * \fn operator> (const Date &) const
     * \brief Compare the date
     * \return true if the Date is greater than this.
     *
     */
    bool operator>(const Date &) const;

    /*!
     *
     * \fn operator<= (const Date &) const
     * \brief Compare the date
     * \return true if the Date is lower or equal than this.
     *
     */
    bool operator<=(const Date &) const;

    /*!
     *
     * \fn operator>= (const Date &) const
     * \brief Compare the date
     * \return true if the Date is greater or equal than this.
     *
     */
    bool operator>=(const Date &) const;

    /*!
     *
     * \fn operator- (const Date &) const
     * \brief Make the difference between two Dates
     * \return the difference of the two values
     *
     */
    double operator-(const Date &) const;
};

#endif // DATE_HPP
