/**
 *
 * @file src/trace/values/Hex.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 * @author Thibault Soucarre
 * @author Augustin Degomme
 * @author Clement Vuchener
 *
 * @date 2024-07-17
 */
#ifndef HEX_HPP
#define HEX_HPP

/*!
 *
 * \file Hex.hpp
 *
 */
/*!
 *
 * \class Hex
 * \brief Store a hexadecimal integer in the trace
 *
 */
class Hex : public Value
{
private:
    unsigned int _value { 0 };

public:
    /*!
     * \brief Constructor
     */
    Hex();

    /*!
     * \brief Constructor
     */
    Hex(unsigned int);

    /*!
     * \brief Constructor
     */
    Hex(const std::string &);

    /*!
     *
     * \fn get_value() const
     * \return the value
     *
     */
    unsigned int get_value() const { return _value; }

    /*!
     * \fn to_string() const
     * \return a string of the value
     */
    std::string to_string() const override;
};

#endif // HEX_HPP
