/**
 *
 * @file src/trace/values/String.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Olivier Lagrasse
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
#include <string>
/* -- */
#include "trace/values/Value.hpp"
#include "trace/values/String.hpp"

String::String() :
    _value("") {
    _is_correct = true;
}

String::String(std::string value) :
    _value(std::move(value)) {
    _is_correct = true;
}

std::string String::to_string() const {
    return _value;
}

bool String::less_than::operator()(const String &s1, const String &s2) const {
    return s1._value < s2._value;
}

bool String::operator==(const std::string &s) const {
    return (_value == s);
}

String String::operator=(const std::string &s) {
    _value = s;
    return *this;
}
