/**
 *
 * @file src/trace/values/Double.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 * @author Thibault Soucarre
 * @author Clement Vuchener
 *
 * @date 2024-07-17
 */
#ifndef DOUBLE_HPP
#define DOUBLE_HPP

/*!
 *
 * \file Double.hpp
 *
 */
/*!
 *
 * \class Double
 * \brief Store a double in the trace
 *
 */
class Double : public Value
{
private:
    double _value { 0.0 };

public:
    /*!
     * \brief Constructor
     */
    Double();

    /*!
     * \brief Constructor
     */
    Double(double);

    /*!
     * \brief Constructor
     */
    Double(const std::string &value);

    /*!
     *
     * \fn to_string() const
     * \return a string of the double
     */
    std::string to_string() const override;

    /*!
     *
     * \fn get_value() const
     * \return the value
     *
     */
    double get_value() const;

    /*!
     *
     * \fn operator+ (const Double &) const
     * \brief Computes the sum between two Doubles
     * \return a double which is equal to the sum of the two Doubles.
     *
     */
    Double operator+(const Double &) const;

    /*!
     *
     * \fn operator- () const
     * \return a Double which is equal to the opposite of this Double.
     *
     */
    Double operator-() const;

    /*!
     *
     * \fn operator- (const Double &) const
     * \brief Make the difference between two Doubles
     * \return a Double which is equal to the difference of the two values.
     *
     */
    Double operator-(const Double &) const;

    /*!
     *
     * \fn operator< (const Double &) const
     * \brief Compare the date
     * \return true if the Double is lower than this.
     *
     */
    bool operator<(const Double &) const;

    /*!
     *
     * \fn operator> (const Double &) const
     * \brief Compare the date
     * \return true if the Double is greater than this.
     *
     */
    bool operator>(const Double &) const;
};

#endif // DOUBLE_HPP
