/**
 *
 * @file src/trace/DrawTrace.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 *
 * @date 2024-07-17
 */
#include <cassert>
#include <cmath>
#include <iostream>
#include <map>
#include <sstream>
#include <vector>
/* -- */
#include <QObject>
/* -- */
#include "common/common.hpp"
#include "common/Info.hpp"
#include "common/Log.hpp"
#include "common/Message.hpp"
/* -- */
#include "interface/SelectInfo.hpp"
/* -- */
#include "render/GanttDiagram.hpp"
/* -- */
#include "trace/values/Values.hpp"
/* -- */
#include "trace/tree/Interval.hpp"
#include "trace/tree/Node.hpp"
#include "trace/tree/BinaryTree.hpp"
/* -- */
#include "trace/EntityTypes.hpp"
#include "trace/Entitys.hpp"
#include "trace/EntityValue.hpp"
#include "trace/Trace.hpp"
#include "trace/DrawTree.hpp"
// #include "trace/values/Value.hpp"
#include "trace/DrawTrace.hpp"

DrawTrace::DrawTrace() {
    _container_width = _DRAWING_CONTAINER_WIDTH_DEFAULT;
    _container_height = _DRAWING_CONTAINER_HEIGHT_DEFAULT;
    _container_h_space = _DRAWING_CONTAINER_H_SPACE_DEFAULT;
    _container_v_space = _DRAWING_CONTAINER_V_SPACE_DEFAULT;

    _state_height = _DRAWING_STATE_HEIGHT_DEFAULT;
    _state_v_space = _DRAWING_STATE_V_SPACE_DEFAULT;
}

DrawTrace::~DrawTrace() = default;

void print_extra_fields(const std::string &name, const std::map<std::string, Value *> *extra_fields, std::ostream *message_buffer) {
    if (extra_fields != nullptr && !extra_fields->empty()) {
        *message_buffer << "<em>" << name << " extra fields</em><br />";
        for (const auto &extra_field: *extra_fields)
            *message_buffer << "<strong>" << extra_field.first << ":</strong> " << extra_field.second->to_string() << "<br />";
    }
}

void display_link_info(const Link *link, SelectInfo *select_info, const int &index) {
    std::stringstream message_buffer;
    message_buffer.str("");
    message_buffer << "<center><strong>Link</strong></center>"
                   << "<strong>Value:</strong> " << link->get_value()->get_name() << "<br />"
                   << "<strong>Source:</strong> " << link->get_source()->get_Name().to_string() << "<br />"
                   << "<strong>Destination:</strong> " << link->get_destination()->get_Name().to_string() << "<br />"
                   << "<strong>Type:</strong> " << link->get_type()->get_name() << "<br />"
                   << "<strong>Date:</strong> " << link->get_start_time().to_string() << " - " << link->get_end_time().to_string() << "<br />"
                   << "<strong>Duration:</strong> " << link->get_duration() << "<br />";
    print_extra_fields("Link", link->get_extra_fields(), &message_buffer);
    print_extra_fields("Value", link->get_value()->get_extra_fields(), &message_buffer);
    print_extra_fields("Type", link->get_type()->get_extra_fields(), &message_buffer);
    select_info->add_info_tab("Link " + std::to_string(index), message_buffer.str(), link->get_start_time().get_value(), link->get_end_time().get_value());
}

void display_event_info(const Event *event, SelectInfo *select_info, const int &index) {
    std::stringstream message_buffer;
    message_buffer.str("");
    message_buffer << "<center><strong>Event</strong></center>"
                   << "<strong>Value:</strong> " << event->get_value()->get_name() << "<br />"
                   << "<strong>Container:</strong> " << event->get_container()->get_Name().to_string() << "<br />"
                   << "<strong>Type:</strong> " << event->get_type()->get_name() << "<br />"
                   << "<strong>Date:</strong> " << event->get_time().to_string() << "<br />";
    print_extra_fields("Event", event->get_extra_fields(), &message_buffer);
    print_extra_fields("Value", event->get_value()->get_extra_fields(), &message_buffer);
    print_extra_fields("Type", event->get_type()->get_extra_fields(), &message_buffer);
    select_info->add_info_tab("Event " + std::to_string(index), message_buffer.str(), event->get_time().get_value(), event->get_time().get_value());
}
void display_state_info(const State *state, SelectInfo *select_info) {
    std::stringstream message_buffer;
    message_buffer.str("");
    message_buffer << "<center><strong>State</strong></center>"
                   << "<strong>Value:</strong> " << state->get_value()->get_name() << "<br />"
                   << "<strong>Container:</strong> " << state->get_container()->get_Name().to_string() << "<br />"
                   << "<strong>Type:</strong> " << state->get_type()->get_name() << "<br />"
                   << "<strong>Date:</strong> " << state->get_start_time().to_string() << " - " << state->get_end_time().to_string() << "<br />"
                   << "<strong>Duration:</strong> " << state->get_duration() << "<br />";
    print_extra_fields("State", state->get_extra_fields(), &message_buffer);
    print_extra_fields("Value", state->get_value()->get_extra_fields(), &message_buffer);
    print_extra_fields("Type", state->get_type()->get_extra_fields(), &message_buffer);
    select_info->add_info_tab("State", message_buffer.str(), state->get_start_time().get_value(), state->get_end_time().get_value());
}
void display_variable_info(const Variable *variable, const Times &x, SelectInfo *select_info) {
    std::stringstream message_buffer;
    message_buffer.str("");
    message_buffer << "<center><strong>Variable</strong></center>"
                   << "<strong>Container:</strong> " << variable->get_container()->get_Name().to_string() << "<br />"
                   << "<strong>Type:</strong> " << variable->get_type()->get_name() << "<br />"
                   << "<strong>Value:</strong> " << variable->get_value_at(x) << "<br />"
                   << "<strong>Value:</strong> " << variable->get_value_at(x) << "<br />"
                   << "<strong>Min:</strong> " << variable->get_min().get_value() << "<br />"
                   << "<strong>Max:</strong> " << variable->get_max().get_value() << "<br />";
    print_extra_fields("Type", variable->get_type()->get_extra_fields(), &message_buffer);
    select_info->add_info_tab("Variable", message_buffer.str(), x, x);
}

void DrawTrace::fill_link_list(const Container *container, const Element_pos &x, const Element_pos &y, const Element_pos &accuracy, Link::Vector *link_list) {
    const Link::Vector *container_links;
    Link *link;
    double a, b, c; // Equation: ax + by + c = 0
    double x1, x2, y1, y2;

    if (!container)
        return;

    // Browse links
    container_links = container->get_links();
    for (const auto &it: *container_links) {
        link = it;

        double srcpos = _container_positions[link->get_source()];
        double dstpos = _container_positions[link->get_destination()];
        double srcsize = _container_sizes[link->get_source()];
        double dstsize = _container_sizes[link->get_destination()];

        x1 = link->get_start_time().get_value();
        x2 = link->get_end_time().get_value();

        y1 = (srcpos + 0.5 * srcsize) * (_container_height + _container_v_space);
        y2 = (dstpos + 0.5 * dstsize) * (_container_height + _container_v_space);

        // Test if (x, y) is in the counding box made by the link ends
        if (((x1 - accuracy <= x && x <= x2 + accuracy) || (x2 - accuracy <= x && x <= x1 + accuracy)) && ((y1 - accuracy <= y && y <= y2 + accuracy) || (y2 - accuracy <= y && y <= y1 + accuracy))) {
            a = y1 - y2;
            b = x2 - x1;
            c = -(a * x1 + b * y1);

            double e = a * x + b * y + c;

            // Test the distance from (x, y) to the link
            if (e * e / (a * a + b * b) < accuracy) {
                link_list->push_back(link);
            }
        }
    }

    return;
}

void fill_event_list(const Node<Event> *node, const Element_pos &x, const Element_pos &d, std::list<const Event *> *event_list) {

    if (nullptr == node) {
        return;
    }

    const Element_pos t = node->get_element()->get_time().get_value();

    if (t < x) {
        if (x - t < d) {
            fill_event_list(node->get_left_child(), x, d, event_list);
            event_list->push_back(node->get_element());
        }
        fill_event_list(node->get_right_child(), x, d, event_list);
        return;
    }

    fill_event_list(node->get_left_child(), x, d, event_list);
    if (t - x < d) {
        event_list->push_back(node->get_element());
        fill_event_list(node->get_right_child(), x, d, event_list);
    }
    return;
}

void DrawTrace::display_information(SelectInfo *select_info, const Trace *trace, double x, double y, double d) {
    const Container *container = nullptr;
    const Container *ancestor = nullptr;

    Link::Vector link_list;
    std::list<const Event *> event_list;
    const State *state;
    const Variable *variable;

    select_info->clear();

    // find container needs to know the position of each container
    Element_pos yr = y;
    const Container::Vector *root_containers = trace->get_view_root_containers();
    if (root_containers->empty())
        root_containers = trace->get_root_containers();
    if (!root_containers->empty())
        for (const auto &root_container: *root_containers)
            if ((container = search_container_by_position(root_container, yr)))
                break;

    // If the clic is out
    if (!container)
        return;

    // Calculate the container positions
    int position = 0;
    for (const auto &root_container: *root_containers) {
        position += calc_container_positions(root_container, position);
    }
    // Now browsing for the events and states of the container root
    // Verification if there is events or a state in the container
#ifndef USE_ITC
    if ((!container->get_events()->empty() || !container->get_states()->empty()) && yr < _container_height + _container_v_space) {
#else
    if (((container->get_states() != NULL && !container->get_states()->empty()) || (container->get_events() != NULL && !container->get_events()->empty())) && yr < _container_height + _container_v_space) {
#endif
        if ((state = find_state(container, x))) {
            display_state_info(state, select_info);
        }
        if (!Info::Render::_no_events) {
            if (container->get_events() != nullptr) {
                fill_event_list(container->get_events()->get_root(), x, d, &event_list);
                int i = 1;
                for (auto const &event: event_list) {
                    display_event_info(event, select_info, i);
                    i++;
                }
            }
        }
    }
    // Else search for a variable
    else {
#ifdef USE_ITC
        if ((container->get_states() != NULL && !container->get_states()->empty()) || (container->get_events() != NULL && !container->get_events()->empty()))
#else
        if (!container->get_events()->empty() || !container->get_states()->empty())
#endif
            yr -= _container_height + _container_v_space;
        const std::map<VariableType *, Variable *> *variable_map = container->get_variables();
        std::map<VariableType *, Variable *>::const_iterator i = variable_map->begin();
        while (yr > _container_height + _container_v_space) {
            yr -= _container_height + _container_v_space;
            i++;
        }
        if (i != variable_map->end()) {
            variable = (*i).second;
            display_variable_info(variable, (Times)x, select_info);
        }
    }

    // Last we browse to find links
    if (!Info::Render::_no_arrows) {
        ancestor = container;
        while (ancestor) {
            fill_link_list(ancestor, x, y, d, &link_list);
            ancestor = ancestor->get_parent();
        }
        int i = 1;
        for (auto const &link: link_list) {
            display_link_info(link, select_info, i);
            i++;
        }
    }
    select_info->show_window();
    return;
}

const Container *DrawTrace::search_container_by_position(const Container *container, Element_pos &y) {
    const Container *result;
    // Search if the result is a descendant
    const Container::Vector *children = container->get_view_children(); // we want to display only children meant to be displayed
    if (children->empty())
        children = container->get_children();
    for (const auto &i: *children) {
        if ((result = search_container_by_position(i, y)) != nullptr)
            return result;
    }

    // Calculate the size of the container (without its children)
    int size = 0;
#ifdef USE_ITC
    if ((container->get_states() != NULL && !container->get_states()->empty()) || (container->get_events() != NULL && !container->get_events()->empty()))
#else
    if (!container->get_states()->empty() || !container->get_events()->empty())
#endif
        size++;
    if (container->get_variable_number() > 0)
        size += container->get_variable_number();

    if (children->empty() && size < 1) {
        // This container has no child and nothing to display (no state, variable or events)

        size = 1; // It still counts for 1 container

        /* In such case, the container has a height of only _container_height,
         * while when a container is not empty, it has a height of
         * container_height + _container_vspace.
         * We need to compensate the missing _container_vspace, by adding it to y
         * (rather than including it in the size variable -- which would
         * require to convert size to a float and have floating
         * imprecisions...).
         * This addition is also required for the next y substraction to be
         * correct if the position is not in this container. */
        y += _container_v_space;
    }

    // Test if the position is in this container
    if (y < size * (_container_height + _container_v_space))
        return container;
    else
        // Valid even for empty containers since we added the missing _container_v_space above
        y -= size * (_container_height + _container_v_space);

    // The position is outside this container
    return nullptr;
}

int DrawTrace::calc_container_positions(const Container *container, int position) {
    int size = 0;

    // Draw children
    const Container::Vector *children = container->get_view_children(); // we want to display only children meant to be displayed
    if (children->empty())
        children = container->get_children();
    for (const auto &i: *children) {
        size += calc_container_positions(i, position + size);
    }

    // Store the position to draw links
    _container_positions[container] = position; // First line after children
    _container_sizes[container] = size; // First line after children

    // Use one line for states and events
#ifdef USE_ITC
    if ((container->get_states() != NULL && !container->get_states()->empty()) || (container->get_events() != NULL && !container->get_events()->empty())) {
#else
    if (!container->get_states()->empty() || !container->get_events()->empty()) {
#endif
        size++;
    }

    // Use one line for each variable
    if (container->get_variable_number() > 0) {
        size += container->get_variable_number();
    }

    return size;
}

const State *DrawTrace::find_state(const Container *container, Element_pos x) {
    if (!container || container->get_states() == nullptr)
        return nullptr;

    Node<StateChange> *node = container->get_states()->get_root();

    while (node) {
        Element_pos t = node->get_element()->get_time().get_value();
        if (x < t) {
            if (node->get_element()->get_left_state() && node->get_element()->get_left_state()->get_start_time().get_value() < x)
                return node->get_element()->get_left_state();
            node = node->get_left_child();
        }
        else {
            if (node->get_element()->get_right_state() && x < node->get_element()->get_right_state()->get_end_time().get_value())
                return node->get_element()->get_right_state();
            node = node->get_right_child();
        }
    }

    return nullptr;
}

bool DrawTrace::link_is_in_set(Link *link, std::vector<const Container *> *set_container, Interval *interval) {
    const Container *src = link->get_source();
    const Container *dest = link->get_destination();
    for (unsigned int i = 0; i < set_container->size(); i++) {
        if (src == (*set_container)[i]) {
            for (const auto &j: *set_container)
                if (dest == j)
                    if (
                        (interval->_left.get_value() < link->get_start_time().get_value() && interval->_right.get_value() > link->get_end_time().get_value()) || (interval->_left.get_value() < link->get_end_time().get_value() && interval->_right.get_value() > link->get_start_time().get_value()))
                        return true;
        }
    }
    return false;
}

bool DrawTrace::is_in_set(const Container *c, std::vector<const Container *> *set_container) {
    if (!c || !set_container)
        return false;
    for (const auto &i: *set_container)
        if (c == i)
            return true;
    return false;
}

void DrawTrace::add(std::vector<const Container *> *container, std::stack<Container *> *containers) {

    while (!containers->empty()) {
        const Container *c = containers->top();
        containers->pop();
        const Container::Vector *children = c->get_view_children(); // we want to display only children meant to be displayed
        if (children->empty())
            children = c->get_children();
        for (const auto &i: *children) {
            containers->push(i);
            container->push_back(i);
        }
        add(container, containers);
    }
}

void DrawTrace::build(GanttDiagram *draw_object, Trace *trace) {
    double zoom = trace->get_filter();
    Interval *interval; // = trace->get_interval_constrained();
    // clear entities
    _container_positions.clear();
    _container_sizes.clear();
    _entity_containers.clear();
    _link_containers.clear();
    _variable_containers.clear();

    // Set the trace length inside the Geometry of the GanttDiagram for scaling
    draw_object->set_trace_length((float)trace->get_max_date().get_value());

    thread_local std::vector<const Container *> local_container;
    local_container.clear();
    std::vector<const Container *> *container = trace->get_selected_container();
    if ((container == nullptr) || container->empty()) {
        if (!container)
            container = &local_container;
        container->clear();

        trace->set_filter(0);

        std::stack<Container *> containers;
        const Container::Vector *root_containers = trace->get_view_root_containers();
        if (root_containers->empty())
            root_containers = trace->get_root_containers();
        for (const auto &root_container: *root_containers) {
            containers.push(root_container);
            container->push_back(root_container);
        }
        add(container, &containers);
    }
    // Adding the parent containers if not added yet
    else {
        bool ended = false;
        std::vector<const Container *> store;
        store.clear();
        while (!ended) {
            ended = true;
            for (std::vector<const Container *>::const_iterator it = container->begin();
                 it != container->end();
                 it++) {

                if (*it && (*it)->get_parent())
                    if (!is_in_set((*it)->get_parent(), container)) {
                        store.push_back((*it)->get_parent());
                        ended = false;
                    }
            } // end for
            // TODO Warning the same container should be added more than 1 time
            for (std::vector<const Container *>::const_iterator it = store.begin();
                 it != store.end();
                 it++) {
                if (!is_in_set(*it, container))
                    container->push_back(*it);
            }
            store.clear();
        } // end while
    } // end else

    interval = trace->get_interval_constrained();

    draw_object->start_draw();

    draw_object->start_draw_containers();
    browse_container_tree(draw_object, trace, container);
    draw_object->end_draw_containers();
    browse_entities(draw_object, zoom, interval, container);

    draw_object->end_draw();
}

void DrawTrace::browse_container_tree(GanttDiagram *draw_object, Trace *trace, std::vector<const Container *> *container) {
    /*** Drawing containers ***/
    Element_pos y = 0.;

    const Container::Vector *root_containers = trace->get_view_root_containers();
    if (root_containers->empty())
        root_containers = trace->get_root_containers();

    if (root_containers->empty()) {
        *Message::get_instance() << QObject::tr("There is no container. The trace can not be drawn.").toStdString() << Message::endw;
    }
    else {
        for (const auto &root_container: *root_containers)
            // Use the fact that if a container is selected, then it implies that all his ancestors are
            if (is_in_set(root_container, container))
                y += browse_container(draw_object, root_container, 0., y, _container_width, 0., container) + _container_v_space;
    }
}

Element_pos DrawTrace::browse_container(GanttDiagram *draw_object, const Container *container, Element_pos x, Element_pos y,
                                        Element_pos w, Element_pos h, std::vector<const Container *> *set_container) {

    // Draw children:
    // we want to display only children meant to be displayed
    const Container::Vector *children = container->get_view_children();
    if (children->empty())
        children = container->get_children();

    for (const auto &i: *children) {
        h += browse_container(draw_object, i, x + _container_width + _container_h_space, y + h, _container_width, 0., set_container) + _container_v_space;
    }

    // Use one line for states and events
#ifdef USE_ITC
    if ((container->get_states() != NULL && !container->get_states()->empty()) || (container->get_events() != NULL && !container->get_events()->empty()))
#else
    if (!container->get_states()->empty() || !container->get_events()->empty())
#endif
    {
        h += _container_height + _container_v_space;
        _entity_containers.push_back(container);
    }

    // Store the position to draw links
    _container_positions[container] = y;
    _container_sizes[container] = h;

    // Use one line for each variable
    if (container->get_variable_number() > 0) {
        _variable_containers.push_back(container);
        h += (_container_height + _container_v_space) * container->get_variable_number();
    }

    if (h < _container_height) // Minimum size
        h = _container_height;

    // Push containers with links to draw
    if (!container->get_links()->empty())
        _link_containers.push_back(container);

    h -= _container_v_space;

    // Draw this container
    draw_object->draw_container(x, y + _container_v_space / 2, w, h, container->get_Name().to_string());

    return h;
}

void DrawTrace::browse_entities(GanttDiagram *draw_object, double zoom, Interval *interval, std::vector<const Container *> *set_container) {
    //////////////////////////////////////////////////////////
    const Container *container;
    StateChange::Tree *state_tree;
    Event::Tree *event_tree;
    const Link::Vector *link_list;
    Link *link;
    const std::map<VariableType *, Variable *> *variable_map;
    Variable *var;
    const std::list<std::pair<Date, Double>> *variable_values;
    Element_pos position;
    std::map<std::string, Value *>::const_iterator field;
    Element_pos lvl_zoom;

    if (zoom >= 0)
        lvl_zoom = zoom;
    else
        lvl_zoom = 0;

    draw_object->start_draw_states();
    draw_object->start_draw_events();
    for (std::list<const Container *>::const_iterator c = _entity_containers.begin();
         c != _entity_containers.end();
         c++) {
        if (is_in_set(*c, set_container)) {
            container = *c;

            position = _container_positions[container] + _container_sizes[container] - (_container_height + _container_v_space);

            state_tree = container->get_states();
            event_tree = container->get_events();
#ifdef USE_ITC
            if (state_tree != NULL) {
#endif
                if (!state_tree->empty()) {
                    // printf("drawing states for %s\n", container->get_Name().to_string().c_str());
                    //  Browse states
                    DrawTree<GanttDiagram, StateChange>(draw_object, position, lvl_zoom,
                                                        _container_height, _container_v_space, _state_height, _state_v_space)
                        .draw_tree(state_tree, *interval);
                }
#ifdef USE_ITC
            }
            if (event_tree != NULL) {
#endif
                if (!event_tree->empty()) {
                    // printf("drawing events for %s\n", container->get_Name().to_string().c_str());
                    //  Browse events
                    DrawTree<GanttDiagram, Event>(draw_object, position, lvl_zoom,
                                                  _container_height, _container_v_space, _state_height, _state_v_space)
                        .draw_tree(event_tree, *interval);
                }
#ifdef USE_ITC
            }
#endif
        }
    } /* end for (!_stack_states.empty()) */

    draw_object->end_draw_events();
    draw_object->end_draw_states();

    draw_object->start_draw_arrows();

    for (std::list<const Container *>::const_iterator c = _link_containers.begin();
         c != _link_containers.end();
         c++) {
        if (is_in_set(*c, set_container)) {
            container = *c;

            // Browse links
            link_list = container->get_links();
            for (const auto &it: *link_list) {
                link = it;
                EntityValue *value = link->get_value();
                assert(value);
                const Color *color = value->get_used_color();
                assert(color);
                if (link_is_in_set(link, set_container, interval))
                    draw_link(draw_object, link, color, value);
            } // end for
        } /* end while (!_stack_states.empty()) */
    }

    draw_object->end_draw_arrows();
    draw_object->start_draw_counter();

    for (std::list<const Container *>::const_iterator c = _variable_containers.begin();
         c != _variable_containers.end();
         c++) {

        if (is_in_set(*c, set_container)) {

            container = *c;
            position = _container_positions[container] + _container_sizes[container];

            // Browse variables
            variable_map = container->get_variables();

            for (const auto &i: *variable_map) {

                var = i.second;
                double min = var->get_min().get_value();
                double max = var->get_max().get_value();
                variable_values = var->get_values();

                double first_value = 0.;
                double second_value = 0.;

                draw_object->draw_text_value((long int)var, 0.0, position + 0.5 * _container_height + _container_v_space / 2);
                _var_positions[var] = position;
                draw_variable_value(draw_object, 0.0, 0.0, position);
                for (const auto &variable_value: *variable_values) {

                    /* Call the object state drawing function.
                     We pass the first value if correspond to the beginning */
                    first_value = variable_value.first.get_value();

                    // min and max can be equal if all values are equal (ie 0) or if there is only one value for the variable
                    if (fabs(max - min) < std::numeric_limits<double>::epsilon()) {
                        second_value = 0;
                    }
                    else {
                        second_value = (variable_value.second.get_value() - min) / (max - min);
                    }

                    if (!(first_value == 0. && second_value == 0.)) {
                        draw_variable_value(draw_object, first_value, second_value, position);
                    }
                }
                draw_variable_value(draw_object, 0.0, 0.0, position);
                position += _container_height + _container_v_space; // One line was used
            } /* end for */
        }
    } // end for

    draw_object->end_draw_counter();
}

void DrawTrace::draw_variable_value(GanttDiagram *draw_object, double time, double value, Element_pos position) {
    Element_pos y = position + (_container_height + _container_v_space) - _container_v_space / 2 - value * _container_height;
    draw_object->draw_counter(time, y);
}

void DrawTrace::draw_link(GanttDiagram *draw_object, const Link *link, const Color *color, EntityValue *value) {
    double starttime = link->get_start_time().get_value();
    double endtime = link->get_end_time().get_value();
    double srcpos = _container_positions[link->get_source()];
    double dstpos = _container_positions[link->get_destination()];
    double srcsize = _container_sizes[link->get_source()];
    double dstsize = _container_sizes[link->get_destination()];

    Element_pos y1 = (srcpos + 0.5 * srcsize); //*(_container_height+_container_v_space);
    Element_pos y2 = (dstpos + 0.5 * dstsize); //*(_container_height+_container_v_space);

    if (color != nullptr)
        draw_object->draw_arrow(starttime, endtime, y1, y2, color->get_red(), color->get_green(), color->get_blue(), value);
    else /* Draw white link */
        draw_object->draw_arrow(starttime, endtime, y1, y2, 1.0, 1.0, 1.0, value);
}
