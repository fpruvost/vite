/**
 *
 * @file src/trace/Event.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 * @author Thibault Soucarre
 * @author Clement Vuchener
 *
 * @date 2024-07-17
 */

#include <string>
#include <map>
#include <list>
#include <vector>
#include <stack>
/* -- */
#include "trace/values/Values.hpp"
#include "trace/EntityTypes.hpp"
#include "trace/Entitys.hpp"
/* -- */
using namespace std;

Event::Event(Date time, EventType *type, Container *container, EntityValue *value, map<std::string, Value *> opt) :
    Entity(container, std::move(opt)), _time(std::move(time)), _type(type), _value(value) {
}

Date Event::get_time() const {
    return _time;
}

const EventType *Event::get_type() const {
    return _type;
}

EntityValue *Event::get_value() const {
    return _value;
}
