/**
 *
 * @file src/trace/DrawTrace.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Olivier Lagrasse
 * @author Mathieu Faverge
 * @author Francois Trahay
 * @author Thibault Soucarre
 * @author Kevin Coulomb
 * @author Philippe Swartvagher
 * @author Luca Bourroux
 * @author Augustin Degomme
 * @author Clement Vuchener
 *
 * @date 2024-07-17
 */
/*!
 *\file DrawTrace.hpp
 */

#ifndef DRAW_TRACE_HPP
#define DRAW_TRACE_HPP

#include <cmath>
/* -- */
#include "interface/SelectInfo.hpp"
/* -- */
#include "render/Render_windowed.hpp"
/* -- */
#include "trace/Container.hpp"
#include "trace/DrawTree.hpp"
#include "trace/EntityValue.hpp"
#include "trace/Event.hpp"
#include "trace/StateChange.hpp"
#include "trace/Trace.hpp"
#include "trace/Variable.hpp"
#include "common/Message.hpp"

class Container;
class EntityValue;
class GanttDiagram;
class Interface_graphic;
class Interval;
class Trace;
class State;
class Variable;

/*
 * Theses constants can not be put as static const float because it is a template and there binary representation is not normed by the C++ langage.
 */

/*!
 * \def _DRAWING_CONTAINER_HEIGHT_DEFAULT
 * \brief The default height for basic containers.
 */
#define _DRAWING_CONTAINER_HEIGHT_DEFAULT 1.2f

/*!
 * \def _DRAWING_CONTAINER_WIDTH_DEFAULT
 * \brief The default width for containers.
 */
#define _DRAWING_CONTAINER_WIDTH_DEFAULT 2.5f

/*!
 * \def _DRAWING_CONTAINER_H_SPACE_DEFAULT
 * \brief The default horizontal space between containers.
 */
#define _DRAWING_CONTAINER_H_SPACE_DEFAULT 0.1f

/*!
 * \def _DRAWING_CONTAINER_V_SPACE_DEFAULT
 * \brief The default vertical space between containers.
 */
#define _DRAWING_CONTAINER_V_SPACE_DEFAULT 0.2f

/*!
 * \def _DRAWING_STATE_HEIGHT_DEFAULT
 * \brief The default height for states.
 */
#define _DRAWING_STATE_HEIGHT_DEFAULT 1.2f

/*!
 * \def _DRAWING_STATE_V_SPACE_DEFAULT
 * \brief The default vertical space between states.
 */
#define _DRAWING_STATE_V_SPACE_DEFAULT 0.2f

/*!
 * \class DrawTrace
 * \brief Browse the trace and call back GantDiagram drawing methods or simply get information on entity
 */
class DrawTrace
{

protected:
    /*!
     * \brief Containers with states or events
     */
    std::list<const Container *> _entity_containers;

    /*!
     * \brief Containers with links
     */
    std::list<const Container *> _link_containers;

    /*!
     * \brief Containers with variables
     */
    std::list<const Container *> _variable_containers;

    std::map<const Container *, Element_pos, std::less<const Container *>> _container_positions;
    std::map<const Container *, Element_pos, std::less<const Container *>> _container_sizes;
    std::map<const Variable *, Element_pos> _var_positions;

    // Geometrical information about the trace shape.
    /*!
     * \brief  _container_width width of the container
     */
    Element_pos _container_width;
    /*!
     * \brief _container_height height of the container
     */
    Element_pos _container_height;
    /*!
     * \brief _container_h_space Horizontal space beetween 2 containers
     */
    Element_pos _container_h_space;
    /*!
     * \brief _container_v_space Vertical space between 2 containers
     */
    Element_pos _container_v_space;
    /*!
     * \brief _state_height Height of the state
     */
    Element_pos _state_height;
    /*!
     * \brief _state_v_space Vertical space between 2 states
     */
    Element_pos _state_v_space;

public:
    /***********************************
     *
     * Constructor and destructor.
     *
     **********************************/

    /*!
     * \brief The default constructor
     */
    DrawTrace();

    /*!
     * \brief The destructor
     */
    virtual ~DrawTrace();

    /***********************************
     *
     * Building functions.
     *
     **********************************/

    /*!
     * \fn build(T* draw_object, Trace* trace)
     * \brief The trace building function that do not draw all the containers but only a part.
     * \param draw_object the kind of object which will be drawn (OpenGL, SVG...).
     * \param trace the trace data.
     */
    void build(GanttDiagram *draw_object, Trace *trace);

    /*!
     * \brief Browse and display information on entities in a range d from where the user has clicked
     * \param select_info the SelectInfo window in which the information are displayed
     * \param trace Trace to browse
     * \param x x coordinate of the click
     * \param y y coordinate of the click
     * \param d distance from the click, containing the entities
     */
    void display_information(SelectInfo *select_info, const Trace *trace, double x, double y, double d);

    /*!
     * \brief Return the closest container from the a designated container located on height y
     * Used by Interface_graphic to switch containers
     * \param container base container
     * \param y height of the container to find
     */
    const Container *search_container_by_position(const Container *container, Element_pos &y);

private:
    /***********************************
     *
     * Browsing functions.
     *
     **********************************/
    /*!
     * \fn browse_container_tree
     * \brief Function that browses the containers of the trace argument that
     * are in the set container and make them painted with a T object
     */
    void browse_container_tree(GanttDiagram *draw_object, Trace *trace, std::vector<const Container *> *container);

    /*!
     * \fn browse_container
     * \brief Recursive function that browse a container to draw it if it is in
     * the set of container with a T painting object in position, knowing the
     * current depth in the tree
     */
    Element_pos browse_container(GanttDiagram *draw_object, const Container *container, Element_pos x, Element_pos y,
                                 Element_pos w, Element_pos h, std::vector<const Container *> *set_container);

    /*!
     * \brief Browse the states list and draw them
     */
    void browse_entities(GanttDiagram *draw_object, double zoom, Interval *interval, std::vector<const Container *> *set_container);

    /*!
     * \brief Draw a point of a variable curve
     * \param draw_object Object that contains the drawing methods
     * \param time Time of the point
     * \param value Value of of the variable (between 0.0 and 1.0)
     * \param position Line where the variable is drawn
     */
    void draw_variable_value(GanttDiagram *draw_object, double time, double value, Element_pos position);

    /*!
     * \brief Draw a link
     * \param draw_object Object that contains the drawing methods
     * \param starttime Time of the start of the link
     * \param endtime Time of the end of the link
     * \param start Line of the start of the link
     * \param end Line of the end of the link
     * \param color The link color
     */
    void draw_link(GanttDiagram *draw_object, const Link *link, const Color *color, EntityValue *value);

    /*!
     * \brief Recursive function that browse a container to draw it with a T painting object in position, knowing the current depth in the tree
     */
    int calc_container_positions(const Container *container, int position);

    /*!
     * \brief Tries to find all links passing by x and y in the container
     */
    void fill_link_list(const Container *container, const Element_pos &x, const Element_pos &y, const Element_pos &d, Link::Vector *link_list);

    /*!
     * \brief Returns the state at the time x
     */
    const State *find_state(const Container *container, Element_pos x);

    bool link_is_in_set(Link *link, std::vector<const Container *> *set_container, Interval *interval);

    bool is_in_set(const Container *c, std::vector<const Container *> *set_container);

    void add(std::vector<const Container *> *container, std::stack<Container *> *containers);
};
#endif
