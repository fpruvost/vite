/**
 *
 * @file src/trace/EventType.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 * @author Clement Vuchener
 *
 * @date 2024-07-17
 */
#include <string>
#include <list>
#include <map>
/* -- */
#include "trace/values/Values.hpp"
#include "trace/ContainerType.hpp"
#include "trace/EntityType.hpp"
#include "trace/EntityValue.hpp"
#include "trace/EventType.hpp"
/* -- */
using namespace std;

EventType::EventType(const Name &name,
                     ContainerType *container_type,
                     map<std::string, Value *> opt) :
    EntityType(_EntityClass_Event, name,
               container_type, opt) {
    add_value(new EntityValue(name, this, opt));
}
