/**
 *
 * @file src/trace/Link.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 * @author Thibault Soucarre
 * @author Clement Vuchener
 *
 * @date 2024-07-17
 */

#include <string>
#include <map>
#include <list>
#include <vector>
#include <stack>
/* -- */
#include "trace/values/Values.hpp"
#include "trace/EntityTypes.hpp"
#include "trace/Entitys.hpp"
/* -- */
using namespace std;

Link::Link(Date start, Date end,
           LinkType *type, Container *container,
           Container *source, Container *destination,
           EntityValue *value, map<std::string, Value *> opt) :
    Entity(container, std::move(opt)),
    _start(std::move(start)), _end(std::move(end)),
    _type(type), _value(value), _source(source), _destination(destination) {
}

Date Link::get_start_time() const {
    return _start;
}

Date Link::get_end_time() const {
    return _end;
}

double Link::get_duration() const {
    return _end - _start;
}

const LinkType *Link::get_type() const {
    return _type;
}

EntityValue *Link::get_value() const {
    return _value;
}

const Container *Link::get_source() const {
    return _source;
}

const Container *Link::get_destination() const {
    return _destination;
}
