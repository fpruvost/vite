/**
 *
 * @file src/render/RenderLayout.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 *
 * @date 2024-07-17
 */
/*!
 *\file RanderLayout.cpp
 *\brief This is a class containing a render area and the widgets relative to this area
 * There is one RenderLayout per displayed trace
 */

#ifndef _RENDER_LAYOUT_
#define _RENDER_LAYOUT_

#include "interface/Interface_graphic.hpp"
#include "interface/SelectInfo.hpp"
/* -- */
#include "Render_windowed.hpp"
/* -- */
#include <QScrollBar>
#include <QSlider>
#include <QSpinBox>
#include <QVBoxLayout>
/* -- */
class Trace;

class RenderLayout : public QObject
{

    Q_OBJECT

private:
    Interface_graphic *_interface_graphic;

    int _slider_x_position;
    int _slider_y_position;

    QSlider *_scale_container_slider;

    QSpinBox *_zoom_box_x;
    QSpinBox *_zoom_box_y;

    QScrollBar *_scroll_bar_x;
    QScrollBar *_scroll_bar_y;

    Render_windowed *_render_area;

    QPushButton *_goto_start_button;
    QPushButton *_goto_end_button;

    Trace *_trace;

    // This event loop is used to wait for the OpenGL initialization
    QEventLoop _wait_gl_init;
    SelectInfo _select_info_window;

    /*!
     * \brief Contains the conversion factor between x virtual and real scroll unit.
     */
    double _x_factor_virtual_to_real;

    /*!
     * \brief Contains the conversion factor between y virtual and real scroll unit.
     */
    double _y_factor_virtual_to_real;

    /*!
     * \brief Contains the number of unit a page step move in the scroll bar
     * The smoothness of the scroll bar depends on that value
     * Value must be less than 2^31 / _MAX_ZOOM_VALUE, otherwise maximum scroll will overflow on maximum zoom
     */
    static const int _SCROLL_PAGE_STEP = 1000;

public:
    RenderLayout(Interface_graphic *interface_graphic, QWidget *parent_widget, QVBoxLayout *parent_layout, Render_windowed *render_area);

    ~RenderLayout();

    QSlider *get_scale_container_slider() const;

    QSpinBox *get_zoom_box_x() const;
    QSpinBox *get_zoom_box_y() const;

    QScrollBar *get_scroll_bar_x() const;
    QScrollBar *get_scroll_bar_y() const;

    /*!
     * \brief Set the current x value of the zoom box.
     * \param new_value The new zoom value.
     */
    void set_zoom_box_x_value(const int &new_value);

    /*!
     * \brief Set the current y value of the zoom box.
     * \param new_value The new zoom value.
     */
    void set_zoom_box_y_value(const int &new_value);

    Render_windowed *get_render_area();

    void set_trace(Trace *trace);
    Trace *get_trace() const;
    void delete_trace();

    /*!
     * \brief Getter for the select info window
     */
    SelectInfo *get_select_info_window();

    /*!
     * \brief Getter for the QEventLoop wait_gl_init
     */
    QEventLoop *get_wait_init_loop();

    /*!
     * \brief Launch an event loop, waiting for the OpenGL Widget to be initialized
     */
    void initialize_gl_widget();

    /*!
     * \brief Ajust scroll bar values when there is a translation.
     * \param x_value The new x_scroll bar value.
     * \param y_value The new y_scroll bar value.
     */
    void refresh_scroll_bars(const Element_pos &x_value, const Element_pos &y_value);

    /*!
     * \brief Changes goto_start and goto_end icons
     * \param is_along_y_axis True if goto icons should be along the y axis, otherwise it is along the x axis
     */
    void set_axis_icon(const bool is_along_y_axis);

private
    Q_SLOT :

        /*!
         * \brief When the zoom_box_x value is changed, update the render
         * \param new_value : The new zoom x value
         */
        void
        zoom_box_x_value_changed(const int &new_value);

    /*!
     * \brief When the zoom_box_y value is changed, update the render
     * \param new_value : The new zoom y value
     */
    void zoom_box_y_value_changed(const int &new_value);

    void scale_container_value_changed(const int &new_value);

    /*!
     * \brief Change the x position of camera view for state drawing area.
     * \param new_value The new position.
     */
    void scroll_x_value_changed(int new_value);

    /*!
     * \brief Change the x position of camera view for state drawing area.
     * \param new_value The new position.
     */
    void scroll_y_value_changed(int new_value);
};

#endif
