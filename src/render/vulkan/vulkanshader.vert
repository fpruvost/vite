#version 450
#extension GL_ARB_separate_shader_objects : enable

out gl_PerVertex {
    vec4 gl_Position;
};

layout (location = 0) in vec2 in_Pos;
layout (location = 1) in vec3 in_Color;

layout (set=0, binding = 0) uniform MatrixBlock {
	mat4 modelview; // translation et rotation et zoom
	mat4 projection;
};

layout (location = 0) out vec3 color;

void main() {
    gl_Position = projection * modelview * vec4(in_Pos, 0.0, 1.0);
    color = in_Color;
}