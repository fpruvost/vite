/**
 *
 * @file src/render/vulkan/Vk_buffer.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Lucas Guedon
 *
 * @date 2024-07-17
 */

#ifndef VK_BUFFER_HPP
#define VK_BUFFER_HPP

#include <QVulkanWindow>
#include <QVulkanDeviceFunctions>

class Vk_buffer
{

protected:
    VkDevice _dev;
    QVulkanDeviceFunctions *_dev_funcs;
    VkBufferUsageFlagBits _flags;
    uint32_t _memory_index;
    VkDeviceMemory _buf_mem = VK_NULL_HANDLE;
    VkBuffer _buffer = VK_NULL_HANDLE;
    VkDeviceSize _mem_size = 0;

public:
    Vk_buffer(VkBufferUsageFlagBits flags);
    ~Vk_buffer();

    void init(VkDevice dev, QVulkanDeviceFunctions *dev_funcs, uint32_t memory_index);

    void alloc_buffer(VkDeviceSize size);
    void realloc_buffer(VkDeviceSize new_size);

    void free_buffer();

    VkDeviceSize get_mem_size();
};

#endif