/**
 *
 * @file src/parser/TauParser/TauStructs.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
/*!
 * \file TauStructs.hpp
 * \brief This file contains all the structures needed to parse a TAU file.
 */

#ifndef TAUSTRUCTS_HPP
#define TAUSTRUCTS_HPP

namespace Tau {

/*!
 * \struct Container
 * \brief Represents a Tau container
 */
struct Container
{
    unsigned int _node_id;
    unsigned int _thread_id;
    std::string _name;
    bool _is_created;

    /*!
     * \fn Container(unsigned int node_id = 0, unsigned int thread_id = 0, const std::string name = "")
     * \brief Constructor
     * \param node_id The container node id
     * \param thread_id The container thread id
     * \param name The container name
     */
    Container(unsigned int node_id = 0, unsigned int thread_id = 0, const std::string name = "") :
        _node_id(node_id), _thread_id(thread_id), _name(name), _is_created(false) { }
};

/*!
 * \struct State
 * \brief Represents a Tau state
 */
struct State
{
    unsigned int _id;
    unsigned int _state_group;
    std::string _name;
    bool _is_created;

    /*!
     * \fn State(unsigned int id = 0, unsigned int parentGroup = 0, const std::string name = "")
     * \brief Constructor
     * \param id The state id
     * \param parentGroup The parent group id
     * \param name The state name
     */
    State(unsigned int id = 0, unsigned int parentGroup = 0, const std::string name = "") :
        _id(id), _state_group(parentGroup), _name(name), _is_created(false) { }
};

/*!
 * \struct StateGroup
 * \brief Represents a Tau stateType
 */
struct StateGroup
{
    unsigned int _id;
    std::string _name;
    /*!
     * \fn StateGroup(unsigned int id = 0, const std::string name = "")
     * \brief Constructor
     * \param id The state group id
     * \param name The state group name
     */
    StateGroup(unsigned int id = 0, const std::string name = "") :
        _id(id), _name(name) { }
};

/*!
 * \struct Event
 * \brief Represents a Tau event
 */
struct Event
{
    unsigned int _id;
    std::string _name;
    unsigned int _monotonically_increasing;

    /*!
     * \fn Event(unsigned int id = 0, const std::string name = "", unsigned int monotonicallyIncreasing = 0)
     * \brief Constructor
     * \param id The state group id
     * \param name The state group name
     * \param monotonicallyIncreasing unused
     */
    Event(unsigned int id = 0, const std::string name = "", unsigned int monotonicallyIncreasing = 0) :
        _id(id), _name(name), _monotonically_increasing(monotonicallyIncreasing) { }
};
}

#endif // TAUSTRUCTS_HPP
