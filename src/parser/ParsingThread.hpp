/**
 *
 * @file src/parser/ParsingThread.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Augustin Degomme
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
#ifndef PARSING_THREAD_HPP
#define PARSING_THREAD_HPP

#include <QThread>

class Trace;
class Parser;

/*!
 * \class ParsingThread
 * \brief Thread to parse asynchronously a trace with any parser available
 */
class ParsingThread : public QThread
{
    Q_OBJECT

private:
    Parser *_parser;
    Trace *_trace;

public:
    /*!
     *  \fn ParsingThread(Parser *p, Trace *t)
     *  \param p the parser used to parse the file.
     *  \param t the trace where we store data.
     */
    ParsingThread(Parser *p,
                  Trace *t);

    /*!
     *  \fn run()
     *  \brief ends the thread.
     */
    void finish_build();

public Q_SLOTS:
    /*!
     *  \fn run()
     *  \brief run the thread.
     */
    void run();

    /*!
     *  \fn dump(const std::string &path, const std::string &filename)
     *  \brief dumps the remaining IntervalOfContainer at path path
     */
    void dump(const std::string &path,
              const std::string &filename);
};
#endif // PARSING_THREAD_HPP
