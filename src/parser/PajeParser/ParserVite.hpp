/**
 *
 * @file src/parser/PajeParser/ParserVite.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 * @author Olivier Lagrasse
 * @author Clement Vuchener
 *
 * @date 2024-07-17
 */
/*!
 * \file ParserVite.hpp
 * \brief the implementation of Parser for Vite traces.
 */

#ifndef PARSERVITE_HPP
#define PARSERVITE_HPP

class ParserPaje;
struct PajeLine;

/*!
 *
 * \class ParserVite
 * \brief parse the input data format of Vite.
 *
 */
class ParserVite : public Parser
{
private:
    std::vector<PajeLine> _end_link; // useful for multi-thread. We fill it in the threads and store them at the end of all the thread in the trace.

    ParserVite(const ParserVite &);

public:
    /*!
     *  \fn ParserVite()
     */
    ParserVite();
    ParserVite(const std::string &filename);

    /*!
     *  \fn ~ParserVite()
     */
    ~ParserVite() override;

    /*!
     *  \fn parse(Trace &trace, bool finish_trace_after_parse = true)
     *  \param trace : the structure of data to fill
     *  \param finish_trace_after_parse boolean set if we do not have to finish the trace after parsing
     */
    void parse(Trace &trace,
               bool finish_trace_after_parse = true) override;

    /*!
     *  \fn get_percent_loaded() const
     *  \brief return the size of the file already read.
     *  \return the scale of the size already loaded of the file by the parser. (between 0 and 1)
     */
    float get_percent_loaded() const override;
};

#endif // PARSERVITE_HPP
