/**
 *
 * @file src/parser/PajeParser/mt_ParserEventPaje.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Augustin Degomme
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
/*!
 *\file ParserEventPaje.hpp
 *\brief This file contains the event Paje used by the ParserPaje.
 */

#ifndef MT_PARSEREVENTPAJE_HPP
#define MT_PARSEREVENTPAJE_HPP

/**
 * \class mt_ParserEventPaje
 * \brief Reads Hash Table to find fill the Str -> multithread version
 *
 */

#include <QObject>
#include <QThread>
#include <string>
#include <map>
#include <set>
#include <queue>
#include <list>
/* -- */
#include "common/Errors.hpp"
/* -- */
#include "trace/values/Values.hpp"
#include "trace/EntityValue.hpp"
#include "trace/EntityTypes.hpp"
#include "trace/Entitys.hpp"
#include "trace/Trace.hpp"
/* -- */
#include <fstream>
#include "parser/PajeParser/mt_PajeFileManager.hpp"
#include "parser/PajeParser/PajeDefinition.hpp"
#include "parser/PajeParser/ParserDefinitionPaje.hpp"
#include "trace/TraceBuilderThread.hpp"

class Container;
class ParserDefinitionPaje;

typedef std::map<const String, Container *, String::less_than> Container_map;

class mt_ParserEventPaje : public QObject
{
    Q_OBJECT
private:
    ParserDefinitionPaje *_Definitions;

    Container_map _containers;

    mt_ParserEventPaje(const mt_ParserEventPaje &);

    Trace *_trace;

public:
    mt_ParserEventPaje(ParserDefinitionPaje *Defs);

    ~mt_ParserEventPaje() override;

    void setTrace(Trace *trace);

    /*!
     *  \fn store_event(const PajeLine_t *line, Trace &trace)
     *  \param line the line containing the event.
     *  \param trace where we store the event.
     */
    int store_event(const PajeLine_t *line, Trace &trace, Trace_builder_struct *tb_struct);

Q_SIGNALS:
    void build_trace(int n, Trace_builder_struct *tb_struct);
};

#endif // PARSEREVENTPAJE_HPP
