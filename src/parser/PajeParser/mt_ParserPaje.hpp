/**
 *
 * @file src/parser/PajeParser/mt_ParserPaje.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Augustin Degomme
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
/*!
 * \file ParserPaje.hpp
 * \brief The implementation of Parser for Paje traces.
 */

#ifndef MT_PARSERPAJE_HPP
#define MT_PARSERPAJE_HPP

#include <cstdio>
#include <parser/Parser.hpp>
#include <parser/PajeParser/mt_PajeFileManager.hpp>

#include "parser/PajeParser/BuilderThread.hpp"
#include "trace/TraceBuilderThread.hpp"

class ParserDefinitionPaje;
class mt_ParserEventPaje;
class mt_PajeFileManager;
/*!
 *
 * \class ParserPaje
 * \brief parse the input data format of Paje.
 *
 */
class mt_ParserPaje : public Parser
{

    Q_OBJECT
private:
    ParserDefinitionPaje *_ParserDefinition;
    mt_ParserEventPaje *_ParserEvent;
    mt_PajeFileManager *_file;
    bool _being_processed;

public:
    /*!
     *  \fn ParserPaje()
     */
    mt_ParserPaje();
    mt_ParserPaje(const std::string &filename);

    /*!
     *  \fn ~ParserPaje()
     */
    ~mt_ParserPaje() override;

    /*!
     *  \fn parse(Trace &trace)
     *  \param trace : the structure of data to fill
     *  \param finish_trace_after_parse boolean set if we do not have to finish the trace after parsing
     */
    void parse(Trace &trace,
               bool finish_trace_after_parse = true) override;

    void releasefile();
    /*!
     *  \fn get_percent_loaded() const
     *  \brief return the size of the file already read.
     *  \return the scale of the size already loaded of the file by the parser. (between 0 and 1)
     */
    float get_percent_loaded() const override;

Q_SIGNALS:
    void produced(unsigned int i, PajeLine *line);
    void build_finish();
};

#endif // PARSERPAJE_HPP
