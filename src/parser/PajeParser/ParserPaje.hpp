/**
 *
 * @file src/parser/PajeParser/ParserPaje.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 * @author Francois Trahay
 * @author Thibault Soucarre
 * @author Olivier Lagrasse
 *
 * @date 2024-07-17
 */
/*!
 * \file ParserPaje.hpp
 * \brief The implementation of Parser for Paje traces.
 */

#ifndef PARSERPAJE_HPP
#define PARSERPAJE_HPP

#include <cstdio>

class ParserDefinitionPaje;
class ParserEventPaje;
class PajeFileManager;

/*!
 *
 * \class ParserPaje
 * \brief parse the input data format of Paje.
 *
 */
class ParserPaje : public Parser
{
private:
    ParserDefinitionPaje *_ParserDefinition;
    ParserEventPaje *_ParserEvent;
    PajeFileManager *_file { nullptr };

public:
    /*!
     *  \fn ParserPaje()
     */
    ParserPaje();
    ParserPaje(const std::string &filename);

    /*!
     *  \fn ~ParserPaje()
     */
    ~ParserPaje() override;

    /*!
     *  \fn parse(Trace &trace, bool finish_trace_after_parse = true)
     *  \param trace : the structure of data to fill
     *  \param finish_trace_after_parse boolean set if we do not have to finish the trace after parsing
     */
    void parse(Trace &trace,
               bool finish_trace_after_parse = true) override;

    /*!
     *  \fn get_percent_loaded() const
     *  \brief return the size of the file already read.
     *  \return the scale of the size already loaded of the file by the parser. (between 0 and 1)
     */
    float get_percent_loaded() const override;

    /*!
     * Return the parser of definitions.
     *
     */
    ParserDefinitionPaje *get_parser_def() const;
};

#endif // PARSERPAJE_HPP
