/**
 *
 * @file src/parser/PajeParser/ParserEventPaje.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 * @author Francois Trahay
 * @author Olivier Lagrasse
 *
 * @date 2024-07-17
 */
/*!
 *\file ParserEventPaje.hpp
 *\brief This file contains the event Paje used by the ParserPaje.
 */

#ifndef PARSEREVENTPAJE_HPP
#define PARSEREVENTPAJE_HPP

/**
 * \class ParserEventPaje
 * \brief Reads Hash Table to find fill the Str
 *
 */

class ParserEventPaje
{
private:
    ParserDefinitionPaje *_Definitions;

    std::map<const String, Container *, String::less_than> _containers;

    ParserEventPaje(const ParserEventPaje &);

public:
    ParserEventPaje(ParserDefinitionPaje *Defs);
    ~ParserEventPaje();

    /*!
     *  \fn store_event(const PajeLine_t *line, Trace &trace)
     *  \param line the line containing the event.
     *  \param trace where we store the event.
     */
    void store_event(const PajeLine_t *line, Trace &trace);
};
#endif // PARSEREVENTPAJE_HPP
