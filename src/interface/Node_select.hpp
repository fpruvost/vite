/**
 *
 * @file src/interface/Node_select.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 * @author Thibault Soucarre
 * @author Augustin Degomme
 *
 * @date 2024-07-17
 */
/*!
 *\file Stats_window.hpp
 */

#ifndef NODE_SELECT_HPP
#define NODE_SELECT_HPP

class Node_select;

/* For moc compilation */
#include <string>
#include <map>
#include <list>
/* -- */
#include <QWidget>
#include "ui_node_select.h"

#include "trace/values/Values.hpp"
#include "trace/EntityValue.hpp"
#include "trace/EntityTypes.hpp"
#include "trace/Entitys.hpp"
#include "trace/tree/Interval.hpp"
#include "trace/Trace.hpp"

#include "interface/Interface_graphic.hpp"

class QDomElement;
class QDomDocument;
/* -- */

/*!
 * \class Node select
 * \brief Class used to select which containers should be displayed
 *
 */

class Node_select : public QWidget, protected Ui::node_selector
{

    Q_OBJECT
    friend class Interface_graphic;

private:
    std::vector<Container *> _initial_containers;
    std::list<Container *> _displayed_containers;
    std::string _file_viewed;
    Interface_graphic *_console;

    bool _sort_order;

public:
    /*!
     * Default constructor
     * \param parent The parent widget of the window.
     */
    Node_select(Interface_graphic *console, QWidget *parent = nullptr);

    ~Node_select() override;

    /*!
     * \fn set_initial_container_names()
     * \brief Set the names of the containers in the original containers trees, from the trace _root_containers
     */
    void set_initial_container_names();

    /*!
     * \fn set_initial_container_names()
     * \brief Set the names of the containers in the displayed containers tree, from the trace _view_root_containers or if empty from _root_containers
     */
    void set_displayed_container_names();

    /*!
     * \fn set_filename(std::string filename)
     * \brief Set the name of the file trace.
     * \param filename The file name.
     */
    void set_filename(std::string filename);

    /*!
     * \fn init_window()
     * \brief Initialise the node selection window : set the container names, expand the lists
     and clear what could have been here before
    */
    void init_window();

    /*!
     * \fn close_window()
     * \brief Properly close the window
     */
    void close_window();

private:
    /*!
     * \fn build_displayed_nodes(std::list<Container *>& displayed_containers)
     * \brief build a displayable array of Node from the tree viewed
     * \param  displayed_containers an empty list of containers to fill
     */
    void build_displayed_nodes(std::list<Container *> &displayed_containers);

    /*!
     * \fn set_container_names_rec()
     * \brief recursive method used to build tree views from the trace
     */
    void set_container_names_rec(QTreeWidgetItem *current_node, Container *current_container, QFlags<Qt::ItemFlag>, bool load_view);

    /*!
     * \fn reassign_children_rec()
     * \brief recursive method used to build trace view from tree views
     */
    void reassign_children_rec(QTreeWidgetItem *item, QFlags<Qt::CheckState> flg);

    /*!
     * \fn export_children_rec()
     * \brief recursive method used to build xml file from tree views
     */
    void export_children_rec(QTreeWidgetItem *item, QDomElement &parent, QDomDocument &doc);
    /*!
     * \brief This functions receives all keyboard events.
     * \param event The event triggered by the keyboard event.
     */
    void keyPressEvent(QKeyEvent *event) override;

private Q_SLOTS:

    void on_reset_button_clicked();
    void on_export_button_clicked();

    void on_load_button_clicked();
    void on_display_button_clicked();
    void on_sort_button_clicked();
};

#endif // NODE_SELECT_HPP
