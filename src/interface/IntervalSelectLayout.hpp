/**
 *
 * @file src/interface/IntervalSelectLayout.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 *
 * @date 2024-07-30
 */

#ifndef INTERVAL_SELECT_LAYOUT_HPP
#define INTERVAL_SELECT_LAYOUT_HPP

#include "interface/RangeSliderWidget.hpp"
/* -- */
#include "trace/tree/Interval.hpp"
/* -- */
#include <QPushButton>
#include <QDoubleSpinBox>
#include <QGroupBox>
#include <QLabel>

class IntervalSelect;
class RenderLayout;

/*!
 * \class IntervalSelectLayout
 * \brief Class containing the set of widgets to select the interval of one RenderLayout
 *
 */

class IntervalSelectLayout : public QGroupBox
{

    Q_OBJECT

private:
    IntervalSelect *_interval_select;
    RenderLayout *_render_layout;

    // Constant widgets
    QLabel _min_spin_box_label;
    QLabel _max_spin_box_label;

    QLabel _min_value_range_slider_label;
    QLabel _max_value_range_slider_label;

    // interactive widgets
    QLabel _trace_end_label;
    QFrame _trace_end_cursor;

    QPushButton _reset_button;

    QDoubleSpinBox _min_spin_box;
    QDoubleSpinBox _max_spin_box;
    RangeSliderWidget _range_slider;

    int _range_span;

    /*!
     * \brief Maximum time reachable by the slider
     */
    double _max_possible_value;

    /*!
     * \brief Boolean preventing _max_possible_value changing when the slider is moved
     */
    bool _is_slider_moving;

public:
    /*!
     * Default constructor
     * \param interval_select The parent, interval select window.
     * \param render_layout The RenderLayout to which this layout is linked
     */
    IntervalSelectLayout(IntervalSelect *interval_select, RenderLayout *render_layout);

    /**
     * \brief Update the layout values with currently displayed trace
     */
    void update_interval_select_from_view();

    /**
     * \brief Reset the layout to default values
     */
    void reset_values();

    /*!
     * \brief Apply changes to the render
     */
    void update_view_from_interval_select();

    /*!
     * \brief Update the values of spinbox and slider with the ones of the render
     * Called if a zoom is triggered outside of the window, when the render zoom state is changed
     */
    void update_values(const Interval &visible_interval);

    /*!
     * \brief Getter for _render_layout
     */
    const RenderLayout *get_render_layout() const;

private Q_SLOTS:
    void on_min_spin_box_value_changed(double value);
    void on_max_spin_box_value_changed(double value);
    void on_sliderMoved(int low, int high);
};

#endif // INTERVAL_SELECT_HPP
