/**
 *
 * @file src/plugin/Plugin.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mohamed Faycal Boullit
 * @author Mathieu Faverge
 * @author Luca Bourroux
 * @author Olivier Lagrasse
 *
 * @date 2024-07-17
 */

#include <map>
/* -- */
#include <QWidget>
/* -- */
#include "trace/values/Values.hpp"
#include "trace/EntityValue.hpp"
#include "trace/EntityTypes.hpp"
#include "trace/Entitys.hpp"
#include "trace/Trace.hpp"
/* -- */
#include "statistics/Statistics_window.hpp"
/* -- */
#include "plugin/Plugin.hpp"

#include "core/Core.hpp"

Plugin *Plugin::new_instance(const std::string &name) {
    Plugin *plugin = nullptr;
    if (name == "Statistics") {
        plugin = new Statistics_window();
    }
    return plugin;
}
