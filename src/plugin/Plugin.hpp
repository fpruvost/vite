/**
 *
 * @file src/plugin/Plugin.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mohamed Faycal Boullit
 * @author Mathieu Faverge
 * @author Olivier Lagrasse
 *
 * @date 2024-07-17
 */

#ifndef PLUGIN_HPP
#define PLUGIN_HPP

class Trace;
class QVariant;
class Render_windowed;

/*!
 * \class Plugin
 * \brief Base class for plugins
 */
class Plugin : public QWidget
{
protected:
    std::string _name;
    Trace *_trace;
    Render_windowed *_render = nullptr;

public:
    /*!
     * \fn Plugin(QWidget *parent = 0)
     * \brief Default constructor
     */
    Plugin(QWidget *parent = nullptr) :
        QWidget(parent), _trace(nullptr) { }
    /*!
     * \fn init()
     * \brief Initialize the plugin
     */
    virtual void init() = 0;
    /*!
     * \fn clear()
     * \brief Clear the plugin
     */
    virtual void clear() = 0;
    /*!
     * \fn execute()
     * \brief The function executed when we push the execute button from the Plugin window.
     */
    virtual void execute() = 0;
    /*!
     * \fn get_name()
     * \brief Return the name of the plugin
     */
    virtual std::string get_name() = 0;
    /*!
     * \fn set_arguments(std::map<std::string, QVariant *>)
     * \brief Set the arguments of the plugin. Not yet sure for the prototype
     */
    virtual void set_arguments(std::map<std::string /*argname*/, QVariant * /*argValue*/>) = 0;

    /*!
     * \fn set_trace(Trace *t)
     * \brief Set the trace.
     */
    virtual void set_trace(Trace *t) { _trace = t; }

    /*!
     *\brief Render setter
     *\param r Set private _render_windowed to r
     */
    virtual void set_render(Render_windowed *r) { _render = r; }

    /*!
     * \fn new_instance(const std::string &name)
     * \brief Create a static Plugin which corresponds to the name.
     * If the name is unknown, return NULL.
     */
    static Plugin *new_instance(const std::string &name);
};

#endif // PLUGIN_HPP
