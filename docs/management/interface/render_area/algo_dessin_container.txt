pile_pos : PILE d'entiers
pile_hauteur : PILE de reels
buf : conteneur
x : reel
y : reel
y_buf : reel


buf <- conteneur_racine
pile_pos.empiler(0)/* la racine */
pile_hauteur.empiler(0)/* hauteur de dessin de la racine */

si  buf.nbFils() <= 1
	x <- #LONGUEUR_CONTENEUR#
sinon
	x <- #HAUTEUR_CONTENEUR#


y_buf <- 0

resteConteneur = vrai

tant que resteConteneur=vrai faire/* au moins un élément */
     
        pos : Entier;

	pos <- pile_pos.depiler()
        
	si pos < buf.nbFils()
		pile_pos.empiler(pos+1)
		pile_pos.empiler(0)/* prepare pour le prochain */

		buf <- buf.fils(pos)

		si buf.nbFils() <= 1
			x <- x + #LONGUEUR_CONTENEUR# + H_espace
       		sinon
			x <- x + #HAUTEUR_CONTENEUR# + H_espace


	sinon /* on a lu tous les fils ou il y en a aucun */

		si buf.nbFils() <= 1

			/* Si on n'a pas de fils, on augmente la hauteur des container */
			si buf.nbFils() = 0
				/* on recupere la valeur de la hauteur -> si aucun ou un fils : hauteur courante */
				y_buf = #HAUTEUR_CONTENEUR#+V_espace
				pile_hauteur.empiler(y_buf)
				y += y_buf/* y contient la hauteur courante, y_buf la hauteur à ajouter */
			/* Si 1 fils, y reste inchange */

		   
		   dessiner horizontalement buf d'une longueur de #LONGUEUR_CONTENEUR# et d'une hauteur de #HAUTEUR_CONTENEUR# à la position x et y (dessin de la pointe superieur droite)
                  x <- x - #LONGUEUR_CONTENEUR# - H_espace

		sinon
			hauteur_buf <- 0

			Pour tous les fils de buf
				hauteur_buf <- hauteur_buf + pile_hauteur.depiler()/* on empile les hauteurs de tous les fils */

			pile_hauteur.empiler(hauteur_buf) /* rempile la hauteur du container courant (pour être utilisé par son père plus tard) */

			dessiner vericalement buf d'une hauteur égale à hauteur_buf et d'une largeur de #HAUTEUR_CONTENEUR# à la position x et y (dessin de la pointe superieur droite)
                       	x <- x - #HAUTEUR_CONTENEUR# - H_espace


		si buf.aPere() = vrai
			buf <- buf.pere()
		sinon
			resteConteneur = faux


