%%
%% This file is part of the ViTE project.
%%
%% This software is governed by the CeCILL-A license under French law
%% and abiding by the rules of distribution of free software. You can
%% use, modify and/or redistribute the software under the terms of the
%% CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
%% URL: "http://www.cecill.info".
%% 
%% As a counterpart to the access to the source code and rights to copy,
%% modify and redistribute granted by the license, users are provided
%% only with a limited warranty and the software's author, the holder of
%% the economic rights, and the successive licensors have only limited
%% liability.
%% 
%% In this respect, the user's attention is drawn to the risks associated
%% with loading, using, modifying and/or developing or reproducing the
%% software by the user in light of its specific status of free software,
%% that may mean that it is complicated to manipulate, and that also
%% therefore means that it is reserved for developers and experienced
%% professionals having in-depth computer knowledge. Users are therefore
%% encouraged to load and test the software's suitability as regards
%% their requirements in conditions enabling the security of their
%% systems and/or data to be ensured and, more generally, to use and
%% operate it in the same conditions as regards security.
%% 
%% The fact that you are presently reading this means that you have had
%% knowledge of the CeCILL-A license and that you accept its terms.
%%
%%
%% ViTE developers are:
%%
%%        - COULOMB Kevin
%%        - FAVERGE Mathieu
%%        - JAZEIX Johnny
%%        - LAGRASSE Olivier
%%        - MARCOUEILLE Jule
%%        - NOISETTE Pascal
%%        - REDONDY Arthur
%%        - VUCHENER Clément 
%%

The render system is the most important \emph{ViTE} module since it provides graphical output of the trace files. Currently, they are two ways to render a trace file: the \emph{direct render} or the \emph{in-file save render}. 


The \emph{Render} class defines several \emph{passives} displaying methods. What is a \emph{passive} displaying method? It is a method which just perform a displaying action (calling \emph{OpenGL} functions or in-file writing function for \emph{SVG} output) according to its parameters.

Thus, theses methods do not call any others functions to browse, for example, the entity tree.



\section{The render opengl}


\subsection{The drawing methods}


\emph{Render\_opengl} uses the \verb?QGLWidget? to display graphics using the OpenGL API. With three Qt functions, the scene is initialized (\verb?initializeGL()?), resized when the main window is resized (\verb?resizeGL()?) and displays the trace (\verb?paintGL()?). Two functions are used to display a trace :
\begin{itemize}
\item \verb?build()? $->$ creates the display of a trace loaded in the data structure.
\item \verb?unbuild()? $->$ releases the scene.
\end{itemize}



\paragraph{The build() function}
\verb?build()? is called when a new trace needs to be drawn. It releases the waiting screen (currently a rabbit turning) and the timer associated with the waiting screen (for animation). 

\paragraph{The unbuild() function}

This function is the complementary of the \verb?build()? function : it releases the trace design and displays the waiting screen (and also restart the timer for the rabbit animation). 

\paragraph{The drawing function}

To enhance the \emph{ViTE} performance the drawing functions - called by the Data Structure while it browse the trace tree - are inlined functions and are defined in \emph{render\_opengl.hpp}. There must be an ordered to respect the OpenGL Display List mecanism. So, each display list should be opened, filled then closed.

Currently, there are three OpenGL Display List used for the render area: the container, state and counter Display Lists.

Nevertheless, arrows and events do not belong to a Display List since they must be drawn according to the current scale. (Else, the triangle of the arrow will be scaled and mask the states under it!)

Following, the list of calls is given and must be respected for each kind of objects else OpenGL errors should be lifted. Thus, for \emph{container} (and also \emph{container text}), \emph{state}, \emph{arrow} and \emph{counter}, the drawing function (i.e. \emph{draw\_*()}) must be called between \emph{start\_draw\_*()} and \emph{end\_draw\_*()}:

\begin{enumerate}

\item start\_draw()

\item \begin{enumerate}
\item start\_draw\_containers()
\item draw\_container()
\item draw\_container\_text()
\item end\_draw\_containers()
\end{enumerate}

\item \begin{enumerate}
\item start\_draw\_states()
\item draw\_state()
\item end\_draw\_states()
\end{enumerate}

\item \begin{enumerate}
\item start\_draw\_arrows()
\item draw\_arrow()
\item end\_draw\_arrows()
\end{enumerate}


\item draw\_event()

\item \begin{enumerate}
\item start\_draw\_counter()
\item draw\_counter()
\item end\_draw\_counter()
\end{enumerate}


\item end\_draw()

\end{enumerate}




\subsection{The scrolling and scaling methods}


One of the most important feature of \emph{ViTE} is to allow user to freely move in the trace render. They are two primitives available to move in the trace: the scrolling and the scaling.


\textbf{NOTE: the origin of render area is at the top-left of the \emph{QGLWidget}.}


\textbf{NOTE: please refer to the \emph{ViTE} API documentation to know each following attribute meanings.}


\begin{itemize}
\item \emph{Scrolling} allows the user to move the image to the left, the right, the top or the bottom in the render area.
\item \emph{Scaling} allows user to resize the image, for example to perform a zoom in particular point.
\end{itemize}

Thus the user can visualize any part of an image trace just with his keyboard and his mouse. Nervertheless, \emph{ViTE} needs to know what kind of action (push on a touch, move the mouse, etc.) the user does. The information is given by \emph{Qt}. Since the \emph{render opengl} is a \emph{Qt} object (a \emph{QGLWidget}), it receives some of these events (like mouse movement over the render area or mouse click). The others (some keyboard event) are caught by the graphical interface since they are bound as a shortcut then send to the render area with the \emph{Qt} signal and slot mecanism.

Now, we just present the different attributes and methods used to perform scrolling and scaling.

 All of them have the same type: \verb?Element_pos?.

\begin{itemize}
\item \begin{itemize}
\item \verb?_screen_width?: the \emph{Qt QGLWidget} width (in pixel).
\item \verb?_screen_height?: the \emph{Qt QGLWidget} height (in pixel).
\end{itemize}

\item \begin{itemize}
\item \verb?_render_width?: the render area width (in OpenGL unit).
\item \verb?_render_height?: the render area height (in OpenGL unit).
\end{itemize}
\end{itemize}

Both of the \verb?_screen_*? and \verb?_render_*? represent for the user the same area in his monitor but with a different point of view.

\begin{figure}[ht!]
\centering
\includegraphics[width=11cm]{images/render_measure}
\caption{The render area dimensions. The both measures (green and orange) are the same but in different units.\label{render_measure}}
\end{figure}

To convert screen units to render units, you have to use \verb?screen_to_render_x()? and \verb?screen_to_render_y()? (please refer to the \emph{ViTE} API documentation for a whole synopsis). To pass from render to screen, user \verb?render_to_screen_x()? and \verb?render_to_screen_y()?.


Moreover, we need to get the trace coordinates to know, for example, from which entity the user has just clicked. So, there are two methods to convert from render units to trace units: \verb?render_to_trace_x()? and \verb?render_to_trace_y()? and also \verb?trace_to_render_x()? and \verb?trace_to_render_y()?.

\bigskip

Another important coordinates are the trace coordinates. There are several attributes to defines a render~:

\bigskip
\textbf{NOTE: all of the following coordinates are in the trace unit. (and not in OpenGL unit)}
 
\begin{itemize}
\item \begin{itemize}
\item \textbf{Container coordinates}
\item \verb?_container_x_min?
\item \verb?_container_x_max?
\item \verb?_container_y_min?
\item \verb?_container_y_max?
\end{itemize}
\item \begin{itemize}
\item \textbf{State coordinates}
\item \verb?_state_x_min?
\item \verb?_state_x_max?
\item \verb?_state_y_min?
\item \verb?_state_y_max?
\end{itemize}
\end{itemize}

\begin{figure}[ht!]
\centering
\includegraphics[width=12cm]{images/render_measure_trace}
\caption{Container and state dimensions in a trace. (in \textbf{trace} unit)\label{render_measure_trace}}
\end{figure}

Finally, the trace entity positions~:
\textbf{NOTE: all of the following coordinates are in the OpenGL unit.}

\begin{itemize}
\item \verb?_default_entity_x_translate?
\item \verb?_ruler_y?
\item \verb?_ruler_height?
\item \verb?_x_scale_container_state?
\end{itemize}

\begin{figure}[ht!]
\centering
\includegraphics[width=12cm]{images/render_measure_area}
\caption{Container and state positions in a trace. (in \textbf{OpenGL} unit)\label{render_measure_area}}
\end{figure}


%TODO: the ruler

\section{Render out Svg file}
\input{render_svg}
