/**
 *
 * @file tests/stubs/Date.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
#ifndef DATE_HPP
#define DATE_HPP

#include <iostream>
#include "Value.hpp"

/*!
 *
 * \file date.hpp
 * \author  NOISETTE
 * \brief Bouchon
 *
 */

class Date : public Value{
private:
    std::string me;

public:
    Date();
    std::string to_string() const;

    /*!
     *
     * \fn instantiate(const std::string &in, Date &out)
     * \brief Convert a string to a Date
     * \param in String to convert
     * \param out Date to be initialized
     * \return true, if the conversion succeeded
     *
     */
    static bool instantiate(const std::string &in, Date &out);

};


#endif // DATE_HPP
