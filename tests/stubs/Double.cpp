/**
 *
 * @file tests/stubs/Double.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
#include "Double.hpp"

Double::Double(){
    me.erase();
}
std::string Double::to_string() const{
    return me;
}

bool Double::instantiate(const std::string &in, Double &out){
    std::cout << "instantiate " << in << " in double" << std::endl;
    return true;
}
