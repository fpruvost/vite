/**
 *
 * @file tests/stubs/Double.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
#ifndef DOUBLE_HPP
#define DOUBLE_HPP

#include <iostream>
#include "Value.hpp"

/*!
 *
 * \file Double.hpp
 * \author  NOISETTE
 * \brief Bouchon
 *
 */

class Double : public Value{
private:
    std::string me;

public:
    Double();
    std::string to_string() const;

    /*!
     *
     * \fn instantiate(const std::string &in, Double &out)
     * \brief Convert a string to a Double
     * \param in String to convert
     * \param out Double to be initialized
     * \return true, if the conversion succeeded
     *
     */
    static bool instantiate(const std::string &in, Double &out);

};


#endif // DOUBLE_HPP
