/**
 *
 * @file tests/render/Render_opengl.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Olivier Lagrasse
 *
 * @date 2024-07-17
 */
#ifndef RENDER_OPENGL_HPP
#define RENDER_OPENGL_HPP
#include<iostream>


class Render_opengl{
public:
    Render_opengl(){
        std::cout << "Render_opengl constructor" << std::endl;
    }

    void drawRect(){
        std::cout << "Render_opengl drawRect" << std::endl;
    }

};

#endif 
