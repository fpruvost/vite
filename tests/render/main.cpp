/**
 *
 * @file tests/render/main.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Olivier Lagrasse
 *
 * @date 2024-07-17
 */
#include<cstdio>
#include<cstdlib>

#include "Trace.hpp"
#include "Render.hpp"
#include "Render_opengl.hpp"



int main(int argc, char** argv){

    Render<Render_opengl> r(new Render_opengl());
    Trace t;
    
    t.build(r);

    return EXIT_SUCCESS;
}
