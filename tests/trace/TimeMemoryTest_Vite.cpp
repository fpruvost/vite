/**
 *
 * @file tests/trace/TimeMemoryTest_Vite.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <map>
#include <list>

/* Global informations */
#include "common/common.hpp"
#ifdef MEMORY_TRACE
#include "common/TraceMemory.hpp"
#endif
/* -- */
#include "common/Memory.hpp"
#include "common/Tools.hpp"
/* -- */
#include "trace/values/Values.hpp"
#include "trace/EntityValue.hpp"
#include "trace/EntityTypes.hpp"
#include "trace/Entitys.hpp"
#include "trace/Trace.hpp"
#include "parser/Parser.hpp"
#include "parser/ParserVite.hpp"


#define MEMORY_WRITE(mem)      ((mem < 1<<10)?mem:((mem < 1<<20 )?mem/(1<<10):((mem < 1<<30 )?(double)mem/(double)(1<<20):(double)mem/(double)(1<<30))))
#define MEMORY_UNIT_WRITE(mem) ((mem < 1<<10)?"o":((mem < 1<<20 )?"Ko":((mem < 1<<30 )?"Mo":"Go")))

/*!
 *\brief The main function of ViTE.
 */

int main(int argc, char **argv) {
    Parser         *parser;
    Trace          *mytrace;
#ifdef MEMORY_TRACE
    FILE           *file;
#endif
    double          timestamp = 0.0;
    
    /* Get the start time */
    timestamp = clockGet();

#ifdef MEMORY_TRACE
    file      = fopen("toto.trace", "w");
    memAllocTrace(file, timestamp, 0);
    trace_start(file, timestamp, 0, -1);
#endif

    /* Initialize Parser handler */
    parser = new ParserVite("test.ept");

    /* New trace handler */
    mytrace = new Trace();
        
    /* Informations beforge parsing */
    fprintf(stdout, "Before Parse \n"
	    "Memory allocated     : %.3g %s\n", 
	    MEMORY_WRITE(memAllocGetCurrent()), 
	    MEMORY_UNIT_WRITE(memAllocGetCurrent()));

    /* Parse the file */
    parser->parse(*mytrace);
 
    /* Informations after parsing */
    fprintf(stdout, "After parse \n"
	    "Time to parse file   : %.3g s \n"
	    "Max Memory allocated : %.3g %s\n" 
	    "Memory allocated     : %.3g %s\n", 
	    (clockGet()-timestamp), 
	    MEMORY_WRITE(memAllocGetMax()),     MEMORY_UNIT_WRITE(memAllocGetMax()),
	    MEMORY_WRITE(memAllocGetCurrent()), MEMORY_UNIT_WRITE(memAllocGetCurrent()));

    delete parser;       

    fprintf(stdout, "After deleting parser and thread \n"
	    "Max Memory allocated : %.3g %s\n"
	    "Memory allocated     : %.3g %s\n", 
	    MEMORY_WRITE(memAllocGetMax()),     MEMORY_UNIT_WRITE(memAllocGetMax()),
	    MEMORY_WRITE(memAllocGetCurrent()), MEMORY_UNIT_WRITE(memAllocGetCurrent()));

    delete mytrace;

    fprintf(stdout, "After deleting trace\n"
	    "Max Memory allocated : %.3g %s\n"
	    "Memory allocated     : %.3g %s\n", 
	    MEMORY_WRITE(memAllocGetMax()),     MEMORY_UNIT_WRITE(memAllocGetMax()), 
	    MEMORY_WRITE(memAllocGetCurrent()), MEMORY_UNIT_WRITE(memAllocGetCurrent()));

#ifdef MEMORY_TRACE
    trace_finish(file, (clockGet()-timestamp), 0, -1);
    memAllocUntrace();
    fclose(file);
#endif

    return EXIT_SUCCESS;
}
