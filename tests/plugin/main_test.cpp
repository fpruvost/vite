/**
 *
 * @file tests/plugin/main_test.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
#include <QApplication>
#include "Plugin.hpp"
#include "PluginWindow.hpp"


int main(int argc, char **argv) {
    
    QApplication app(argc, argv);

    PluginWindow *wind = new PluginWindow();
    
    wind->load_plugin("plug3");

    wind->show();
    return app.exec();
}
