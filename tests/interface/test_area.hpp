/**
 *
 * @file tests/interface/test_area.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Olivier Lagrasse
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
/*!
 * \brief Test of the container drawing
 */

#ifndef TEST_AREA_HPP
#define TEST_AREA_HPP

#include "resource_test.hpp"
#include "../../src/render/render_opengl.hpp"

#endif
