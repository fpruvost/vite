/**
 *
 * @file tests/interface/resource_test.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Olivier Lagrasse
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
/*!
 *\file resource_test.hpp
 *\brief This file gives some common header files for the interface tests.
 */

#ifndef RESOURCE_TEST_HPP
#define RESOURCE_TEST_HPP

#include "../../src/main_resource.hpp"

#endif
