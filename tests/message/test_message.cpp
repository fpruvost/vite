/**
 *
 * @file tests/message/test_message.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
/*!
 * \brief Test error message
 */

#include <cstdlib>

#include "../../src/message/Message.hpp"

using namespace std;

int main(int argc, char** argv){

    // Creation of an interface
    Interface *interface = new Interface();
    
    Message::set_interface(interface);
    
    // Print of an info
    *Message::get_instance() << "this is an information" << Message::endi;

    // Print of a warn
    *Message::get_instance() << "this is a warning" << Message::endw;

    // Print of an error
    *Message::get_instance() << "this is an error" << Message::ende;


    // Liberation of memory
    Message::kill();
    delete interface;


    return EXIT_SUCCESS;
}
