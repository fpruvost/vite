/**
 *
 * @file tests/parser/test_parser_definition.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Pascal Noisette
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
#include "../../src/parser/Line.hpp"
#include "../../src/parser/ParserDefinitionDecoder.hpp"
#include "../stubs/Trace.hpp"
#include <iostream>
#include <fstream>
#include <cstdlib>

using namespace std;

int main(int argc, char** argv){

    ParserDefinitionDecoder *parserdefinition = new ParserDefinitionDecoder();
  
    Line line;
    if (argc<2)
      line.open("trace_to_parse.trace");
    else
      line.open(argv[1]);
   

    int linecount = 0;
    static const string percent = "%";

    while(!line.is_eof()){

	line.newline();

	if(line.starts_with(percent)){
	    parserdefinition->store_definition(line);
            linecount ++;
	}
    }

    parserdefinition->print_definitions();
    cout << "lu :" << linecount << endl;

    delete parserdefinition;

    return EXIT_SUCCESS;
}
