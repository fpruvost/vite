###
#
#  This file is part of the ViTE project.
#
#  This software is governed by the CeCILL-A license under French law
#  and abiding by the rules of distribution of free software. You can
#  use, modify and/or redistribute the software under the terms of the
#  CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
#  URL: "http://www.cecill.info".
#
#    @version 1.2.0
#    @authors COULOMB Kevin
#    @authors FAVERGE Mathieu
#    @authors JAZEIX Johnny
#    @authors LAGRASSE Olivier
#    @authors MARCOUEILLE Jule
#    @authors NOISETTE Pascal
#    @authors REDONDY Arthur
#    @authors VUCHENER Clément
#    @authors RICHART Nicolas
#

set(DISTRIBUTION_hdrs
  Distribution.hpp
)

set(DISTRIBUTION_srcs
  Distribution.cpp
)

add_library(Distribution SHARED ${DISTRIBUTION_srcs})

target_link_libraries(Distribution
  Qt5::Widgets
  Qt5::Core
  )

install(TARGETS Distribution DESTINATION $ENV{HOME}/.vite)
