/**
 *
 * @file plugins/CriticalPath/ParserStats.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 * @author Mohamed Faycal Boullit
 *
 * @date 2024-07-17
 */
// #include <boost/filesystem/operations.hpp>
#include <fstream>
#include <iostream>
#include <vector>

typedef std::pair<double, double> pair;
typedef std::pair<std::string, std::string> stringpair;
typedef std::pair<pair, pair> doublepair;

class ParserStats
{

private:
    std::vector<double> _task_time;
    double _critical_path_length;
    double _max_breadth;
    stringpair _files; // first .dot second .rec
    std::vector<int> _criticalPath;
    pair _mb_interval;
    double _task_count;
    double _computing_volume;

public:
    /*!
     * \brief ParserStats Constructor parses files and uses computing modules to initialize all statistics
     * \param filename1 .dot / DAG file
     * \param filename2 .rec file
     */
    ParserStats(std::string filename1, std::string filename2);

    /*!
     * \brief ParserStats Destructor
     */
    ~ParserStats();

    /*!
     * \brief Critical Path Length getter
     */
    double get_critical_path_length();

    /*!
     * \brief Max Breadth getter
     */
    double get_max_breadth();

    /*!
     * \brief Max Breadth Interval getter
     */
    pair get_max_breadth_interval();

    /*!
     * \brief Task count getter
     */
    double get_task_count();

    /*!
     * \brief Critical Path task Id Vector getter
     */
    std::vector<int> get_critical_path();
};
