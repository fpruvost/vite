/**
 *
 * @file plugins/CriticalPath/DrawStats.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mohamed Faycal Boullit
 *
 * @date 2024-07-17
 */
#include <string>

/*!
 * \brief Computes the number of cpu used in this trace execution
 * \param trace Trace
 */
double get_cpu_count(Trace *trace);

/*!
 * \brief Initializes parameter critical_path_states with the states that correspond to tasks of the critical path, this would be used to delete modifications when checkbox is checked then unchecked
 * \param trace Trace
 * \param critical_path Vector that contain the jobIds of critical path task's
 * \param critical_path_states Vector that will contain the states of the critical path
 */
void get_critical_path_states(Trace *trace, const std::vector<int> &critical_path, std::vector<const State *> &critical_path_states);

/*!
 * \brief Adds links the states of the critical path
 * \param trace Trace
 * \param critical_path_states Vector that contain the states of the critical path
 */
void link_critical_path(Trace *trace, std::vector<const State *> &critical_path_states);