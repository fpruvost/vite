/**
 *
 * @file plugins/CriticalPath/ParserStats.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 * @author Mohamed Faycal Boullit
 *
 * @date 2024-07-17
 */
#include "ParseDAG.hpp"
#include "ParserStats.hpp"

ParserStats::ParserStats(std::string filename1, std::string filename2) {
    // Construct an empty graph and prepare the dynamic_property_maps.
    graph_t graph(0);
    // Construct table for parsing times for Tasks.rec
    std::vector<double> task_time(MINSIZE, 0);

    dynamic_properties dp(ignore_other_properties);
    dp.property("node_id", get(&Vertex::task_name, graph));
    dp.property("label", get(&Vertex::label, graph));
    dp.property("fillcolor", get(&Vertex::fillcolor, graph));
    dp.property("style", get(&Vertex::style, graph));
    dp.property("color", get(&Edge::color, graph));

    // Use ref_property_map to turn a graph property into a property map
    boost::ref_property_map<graph_t *, std::string> gname(get_property(graph, graph_name));
    dp.property("rankdir", gname);
    std::ifstream dot(filename1);

    if (read_graphviz(dot, graph, dp)) {
        parse_task(filename2, task_time); // Parse the tasks file
        _task_time = task_time;
        std::pair<std::pair<double, size_t>, double> ret = critical_path_length(graph, task_time);
        std::pair<double, size_t> length__last_task = ret.first;
        _task_count = ret.second;
        std::pair<double, pair> max_br = max_breadth(graph);

        _critical_path_length = length__last_task.first; // Critical path length
        _max_breadth = max_br.first; // Max breadth
        _mb_interval = max_br.second; // Max breadth interval
        _files.first = std::move(filename1);
        _files.second = std::move(filename2);
        _criticalPath = critical_path(graph, length__last_task.second);
        _computing_volume = 0;

        /******************* Drawing dag *****************
        if (draw_cp_dag){
            boost::filesystem::path p(filename1);
            std::string new_dag_path = p.parent_path().string() +"/cp_"+p.filename().string();
            std::ofstream dotOutput(new_dag_path);
            write_graphviz_dp(dotOutput, graph, dp);
        }
        */
    }
}

ParserStats::~ParserStats() {
}

double ParserStats::get_critical_path_length() {
    return _critical_path_length;
}

double ParserStats::get_max_breadth() {
    return _max_breadth;
}

pair ParserStats::get_max_breadth_interval() {
    return _mb_interval;
}

double ParserStats::get_task_count() {
    return _task_count;
}

std::vector<int> ParserStats::get_critical_path() {
    return _criticalPath;
}
