/**
 *
 * @file plugins/MatrixVisualizer/Windows/MatrixWindow.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Arthur Chevalier
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
#ifndef MATRIX_WINDOW_HPP
#define MATRIX_WINDOW_HPP

#include "../Formats/SymbolMatrix.hpp"
#include "MatrixGLWidget.hpp"

#include <QMainWindow>
#include <QCloseEvent>
#include <QKeyEvent>
#include <QLabel>

#define MATRIX_WINDOW_LENGTH 512
#define MATRIX_WINDOW_HEIGHT 512

class Matrix_window : public QMainWindow
{
public:
    Matrix_window(symbol_matrix_t *matrix, bool m_quadtree_button_checked);
    ~Matrix_window();

protected:
    void closeEvent(QCloseEvent *event);
    void keyPressEvent(QKeyEvent *keyEvent);

private:
    MatrixGLWidget *m_gl;
    QLabel *m_label;
};

#endif
