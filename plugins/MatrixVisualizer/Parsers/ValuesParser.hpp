/**
 *
 * @file plugins/MatrixVisualizer/Parsers/ValuesParser.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
#ifndef VALUES_PARSER_MATRIX_VISUALIZER_HPP
#define VALUES_PARSER_MATRIX_VISUALIZER_HPP

#include "../Formats/SymbolMatrix.hpp"
#include "Parser.hpp"

class ValuesParser : public Parser<symbol_matrix_t>
{
public:
    ValuesParser();

    symbol_matrix_t *parse(const std::string &path, symbol_matrix_t *m);
    float get_percent_loaded() const;

    bool is_finished() const;
    bool is_cancelled() const;

    void set_canceled();
    void finish();

private:
    float m_percentage_loaded;
};

#endif
