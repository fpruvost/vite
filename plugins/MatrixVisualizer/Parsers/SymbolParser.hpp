/**
 *
 * @file plugins/MatrixVisualizer/Parsers/SymbolParser.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Arthur Chevalier
 * @author Johnny Jazeix
 *
 * @date 2024-07-17
 */
#ifndef SYMBOL_PARSER_MATRIX_VISUALIZER_HPP
#define SYMBOL_PARSER_MATRIX_VISUALIZER_HPP

#include "../Formats/SymbolMatrix.hpp"
#include "Parser.hpp"

class SymbolParser : public Parser<symbol_matrix_t>
{
public:
    SymbolParser();

    symbol_matrix_t *parse(const std::string &path, symbol_matrix_t *m);
    float get_percent_loaded() const;

    bool is_finished() const;
    bool is_cancelled() const;

    void set_canceled();
    void finish();

private:
    float m_percentage_loaded;
};

#endif
