/**
 *
 * @file plugins/MatrixVisualizer/Parsers/Readers/Pastix.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Arthur Chevalier
 * @author Johnny Jazeix
 *
 * @date 2024-07-17
 */
#ifndef READER_PASTIX_HPP
#define READER_PASTIX_HPP

#include "../../Formats/SymbolMatrix.hpp"
#include <cstdio>

// Functionnal
int pastix_read_int(FILE *stream, int32_t *value);

// Reader
int pastix_read_symbol(FILE *stream, symbol_matrix_t *matrix);

#endif
